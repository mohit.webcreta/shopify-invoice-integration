<?php

namespace App\Enums;

enum BaseStatusEnum: string
{
  case PUBLISHED = 'published';
  case PENDING = 'pending';
  case DRAFT = 'draft';
}
