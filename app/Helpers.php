<?php


function statusHtml($status)
{
  $html = '';

  switch ($status->name) {
    case 'PENDING':
      $html = '<span class="badge badge-light-danger">Pending</span>';
      break;

    case 'DRAFT':
      $html = '<span class="badge badge-light-info">Draft</span>';
      break;

    default:
      $html = '<span class="badge badge-light-success">Published</span>';
      break;
  }

  return $html;
}


function dateDiff($date2)
{
  $date1 = date('m/d/Y h:i:s a', time());
  $date1_ts = strtotime($date1);
  $date2_ts = strtotime($date2);
  $diff = $date2_ts - $date1_ts;
  return round($diff / 86400);
}
