<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use App\Models\Config;
use App\Models\Plan;
use App\Models\PlanConfig;
use App\Models\StoreLog;
use App\Models\Recipe;
use App\Models\Choice;
use App\Models\RecipeProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (!Auth::check()) {
            return redirect()->route('login');
        } else {
            return redirect()->route('admin.dashboard');
        }
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function authenticate()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        } else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function storeDashboard()
    {
        $count['stores'] = User::count();
        $count['plans'] = Plan::count();
        $count['recipes'] = Recipe::count();
        $count['choices'] = Choice::count();
        $count['products'] = RecipeProduct::count();
        $count['logs'] = StoreLog::count();
        $count['subscription'] = Subscription::count();

        $plans = Plan::all();
        $stores = User::where('role_type','=','store')->orderBy('id','desc')->take('6')->get();

        return view('admin.pages.dashboard', compact('count','plans','stores'));
    }

    // get users list data
    public function getUsersList()
    {
        $users = User::where('role_type', '=', 'store')->get();

        return view('admin.pages.users.list', compact('users'));
    }

    public function getUsersLogById(Request $request, $id)
    {
        $logs = StoreLog::where('user_id', '=', $id)->orderBy('id','DESC')->get();
        $logTotal = count($logs);

        return view('admin.pages.users.store_log', compact('logs','logTotal'));
    }

    public function getConfigData()
    {

        return view('admin.pages.config.edit');
    }

    public function updateConfigData(Request $request, Config $config)
    {

        $input = $request->all();

        if ($image = $request->file('logo')) {
            $destinationPath = 'assets/media/logos/';
            $logoImage = 'logo' . rand(0, 10000) . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $logoImage);
            $input['logo'] = $destinationPath.'/'. $logoImage;
        } else {
            $input['logo'] = $config->logo;
        }

        if ($image = $request->file('favicon')) {
            $destinationPath = 'assets/media/logos/';
            $faviconImage = 'favicon' . rand(0, 10000) . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $faviconImage);
            $input['favicon'] = $destinationPath.'/'. $faviconImage;
        } else {
            $input['favicon'] = $config->favicon;
        }

        $config->update($input);

        return redirect()->route('admin.config.view')->with('success', 'Configuration updated successfully.');

    }

    // get all plans
    public function getAllPlans()
    {
        $plans = Plan::all();

        return view('admin.pages.plans.index', compact('plans'));
    }

    // edit plan
    public function getPlanData(Plan $plan)
    {
        return view('admin.pages.plans.edit', compact('plan'));
    }

    // update plan
    public function updatePlanData(Request $request, Plan $plan)
    {
        $input = $request->all();
        $input['capped_amount'] = $input['price'];
        $input['test'] = array_key_exists('test', $input) ? '1' : '0';
        $plan->update($input);

        $planConfig = PlanConfig::where('plan_id', '=', $plan->id)->first();

        if ($planConfig) {

            $planConfig->number_recipe_allow = $input['number_recipe_allow'] ? $input['number_recipe_allow'] : 0;
            $planConfig->number_choice_allow = $input['number_choice_allow'] ? $input['number_choice_allow'] : 0;
            $planConfig->number_list_layout_allow = $input['number_list_layout_allow'] ? $input['number_list_layout_allow'] : 0;
            $planConfig->number_view_layout_allow = $input['number_view_layout_allow'] ? $input['number_view_layout_allow'] : 0;
            $planConfig->update();
        } else {
            $planConfig = new PlanConfig();
            $planConfig->plan_id = $plan->id;
            $planConfig->number_recipe_allow = $input['number_recipe_allow'] ? $input['number_recipe_allow'] : 0;
            $planConfig->number_choice_allow = $input['number_choice_allow'] ? $input['number_choice_allow'] : 0;
            $planConfig->number_list_layout_allow = $input['number_list_layout_allow'] ? $input['number_list_layout_allow'] : 0;
            $planConfig->number_view_layout_allow = $input['number_view_layout_allow'] ? $input['number_view_layout_allow'] : 0;
            $planConfig->save();
        }

        return redirect()->route('admin.plans.index')->with('success', 'Plan updated successfully.');

    }

    public function accessDeniedPage()
    {
        return view('admin.pages.403-page');
    }

}