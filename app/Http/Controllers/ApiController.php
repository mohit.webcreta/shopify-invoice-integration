<?php

namespace App\Http\Controllers;

use App\Models\Choice;
use App\Models\Images;
use App\Models\Recipe;
use App\Models\Timing;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchActivatedRecipes(Request $request, $name)
    {
        $store = User::where('name', $name)->first();

        // Define the data you want to include in the file
        $recipes = Recipe::where('status', '=', 'published')->where('user_id', '=', $store->id)->with('images')->get();

        return response()->json([
            'items'  => $recipes
        ]);
    }

    public function fetchRecipe(Request $request, $id)
    {
        $recipe = Recipe::where('slug', '=', $id)->with('images', 'choices', 'options', 'products')->first();

        return response()->json([
            'items'  => $recipe
        ]);
    }
}
