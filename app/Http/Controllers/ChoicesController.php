<?php

namespace App\Http\Controllers;

use App\Enums\BaseStatusEnum;
use App\Http\Requests\ChoiceOptionRequest;
use App\Http\Requests\ChoiceOptionUpdateRequest;
use App\Models\Choice;
use App\Models\Option;
use App\Models\StoreLog;
use Illuminate\Http\Request;
use Auth;

class ChoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all data of choice table
        $store = Auth::user();
        $items = Choice::where('user_id', $store->id)->get();
        return view('master.choices.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('master.choices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChoiceOptionRequest $request)
    {
        $choice = new Choice();
        $choice->name = $request->choice_name;
        $choice->user_id = $request->user_id;
        $choice->order = $request->choice_order;
        $choice->status = $request->status;
        $choice->save();

        foreach ($request->choice_options as $item) {
            if ($item['option_name']) {
                $option = Option::where('name', $item['option_name'])->first();
                if ($option == null) {
                    $option = new Option();
                    $option->name = $item['option_name'];
                    $option->order = $item['option_order'] ?: 0;
                    $option->status = BaseStatusEnum::PUBLISHED;
                    $option->save(); // if not then save new data
                } else {
                    $option->name = $item['option_name'];
                    $option->order = $item['option_order'] ?: 0;
                    $option->status = BaseStatusEnum::PUBLISHED;
                    $option->update(); // if yes then update data
                }
                $option->choices()->attach($choice->id);
            }
        }

        // insert log into store
        $log = new StoreLog();
        $log->user_id = $choice->user_id;
        $log->name = "Choice";
        $log->value = 'A <span class="text-success">new</span> choice named <span class="text-info">' . $choice->name . '</span> has been successfully added to our collection.';
        $log->created_at = now()->format('H:i:s');
        $log->save();


        return redirect()->route('choices.index')->with('success', 'Your data has been saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function show(Choice $choice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function edit(Choice $choice)
    {
        //
        return view('master.choices.edit', compact('choice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function update(ChoiceOptionUpdateRequest $request, $id)
    {
        $choice = Choice::findOrFail($id);

        $choice->name = $request->choice_name;
        $choice->order = $request->choice_order;
        $choice->user_id = $request->user_id;
        $choice->status = $request->status;
        $choice->update();


        $choice->options()->sync([]);

        foreach ($request->choice_options as $item) {
            if ($item['option_id'] == null) {
                $option = Option::where('name', $item['option_name'])->first();
                if ($option == null) {
                    $option = new Option();
                    $option->name = $item['option_name'];
                    $option->order = $item['option_order'] ?: 0;
                    $option->status = BaseStatusEnum::PUBLISHED;
                    $option->save(); // if not then save new data
                } else {
                    $option->name = $item['option_name'];
                    $option->order = $item['option_order'] ?: 0;
                    $option->status = BaseStatusEnum::PUBLISHED;
                    $option->update(); // if yes then update data
                }
            } else {
                $option = Option::find($item['option_id']);
                $option->name = $item['option_name'];
                $option->order = $item['option_order'];
                $option->status = BaseStatusEnum::PUBLISHED;
                $option->update(); // if id is exist then update data
            }

            // attach option with choice
            $option->choices()->attach($choice->id);
        }

        // insert log into store
        $log = new StoreLog();
        $log->name = "Choice";
        $log->value = 'An <span class="text-primary">existing</span> choice named <b class="text-info">' . $choice->name . '</b> has been successfully <span class="text-success">updated</span> to our collection.';
        $log->created_at = now()->format('H:i:s');
        $log->save();


        return redirect()->route('choices.index')->with('success', 'Your data has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $choice = Choice::findOrFail($id);
        $choice->options()->sync([]);
        $choice->delete();


        // insert log into store
        $log = new StoreLog();
        $log->user_id = $choice->user_id;
        $log->name = "Choice";
        $log->value = 'A choice named <span class="text-info">' . $choice->name . '</span> has been successfully <span class="text-danger">deleted</span> from our collection.';
        $log->created_at = now()->format('H:i:s');
        $log->save();

        return redirect()->route('choices.index')->with('success', 'Your data has been deleted successfully');
    }
}