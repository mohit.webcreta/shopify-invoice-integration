<?php

namespace App\Http\Controllers;

use App\Models\Choice;
use App\Models\Plan;
use App\Models\Recipe;
use App\Models\Subscription;
use App\Models\User;
use App\Models\StoreLog;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\File;
use OhMyBrew\ShopifyApp\Models\Shop;
use OhMyBrew\ShopifyApp\Facades\ShopifyAPI;

class ConfigController extends Controller
{

    public function storeDashboard()
    {

        $store = Auth::user();

        $count['recipe'] = Recipe::where('user_id', $store->id)->count();
        $count['choice'] = Choice::where('user_id', $store->id)->count();


        if ($store) {
            $user = User::where('name', '=', $store->name)->first();
            if ($user) {
                $subscription = Subscription::where('user_id', $user->id)->with('plan')->first();
                return view('pages.dashboard', compact('subscription', 'user', 'count'));
            } else {
                return redirect()->route('plan');
            }
        } else {
            return redirect()->route('home');
        }
    }


    // store plan details - POST
    public function storePlan(Request $request)
    {
        $shop = Auth::user();

        // get authenticated shop data
        $plans = Plan::where('name', $type)->first();

        $startDate = new DateTime();
        $oneMonthAfterDate = $startDate->add(new DateInterval('P1M'));
        $endDate = $oneMonthAfterDate->format('Y-m-d');


        Subscription::updateOrCreate(
            [
                'plan_id' => $plans->id,
                'user_id' => $shop->id,
                'start_date' => new DateTime(),
                'end_date' => $endDate,
                'status' => 'active',
            ]
        );

        return redirect()->route('home')->with('success', 'Your plan is activated successfully!');
    }

    public function uploadLiquidPage(Request $request, Response $response)
    {
        $store = Auth::user();

        $api_key = 'b3d57acff021998246a747fc259ba6c8';
        $api_pass = 'c42bb585340b21b18ef51ace0241bdd6';

        $themes = $store->api()->rest('GET', '/admin/themes.json');
        $themeId = "";
        // get active theme id
        foreach ($themes['body']['themes'] as $theme) {
            if ($theme->role == "main") {
                $themeId = $theme->id;
            }
        }
        $filePath = public_path('assets/files/recipes.liquid');

        if (file_exists($filePath)) {
            $snippet = file_get_contents($filePath);

            // define page name
            $fileName = 'recipe';
            $liquidName = 'templates/page.' . $fileName . '.liquid';
            $domain = $store->name . '/pages/' . $fileName;

            // check page is exist or not
            $liquidIsExist = $store->api()->rest('GET', '/admin/themes/' . $themeId . '/assets.json', ['asset[key]' => $liquidName]);
            $newLiquid = $liquidIsExist['errors'] ? true : false;

            // check page is exist or not
            if ($newLiquid) {
                $array = array('asset' => array('key' => $liquidName, 'value' => $snippet));
                $response = $store->api()->rest('PUT', '/admin/themes/' . $themeId . '/assets.json', $array);


                // check is there error or not
                if ($response['errors']) {
                    $log = new StoreLog();
                    $log->user_id = $store->id;
                    $log->name = "Recipe Page";
                    $log->value = '<span class="text-danger">Sorry!</span> The page you are trying to add does not created due to server error. Please try again later!';
                    $log->created_at = now()->format('H:i:s');
                    $log->save();


                } else {

                    $pageData = array(
                        "page" => array(
                            "title" => ucfirst($fileName),
                            "handle" => $fileName,
                            "template_suffix" => $fileName,
                        )
                    );

                    $pageResponse = $store->api()->rest('POST', '/admin/api/2023-07/pages.json', $pageData);

                    if ($pageResponse['errors']) {

                        $log = new StoreLog();
                        $log->user_id = $store->id;
                        $log->name = "Recipe Page";
                        $log->value = '<span class="text-danger">Sorry!</span> The page you are trying to access does not showing due to server error. Please try again later!';
                        $log->created_at = now()->format('H:i:s');
                        $log->save();

                    } else {

                        $log = new StoreLog();
                        $log->user_id = $store->id;
                        $log->name = "Recipe Page";
                        $log->value = 'A <span class="text-success">new</span> page named <span class="text-info"> recipes </span> has been successfully created.';
                        $log->created_at = now()->format('H:i:s');
                        $log->save();

                        $log = new StoreLog();
                        $log->user_id = $store->id;
                        $log->name = "Recipe Link";
                        $log->value = '<span class="text-success">Congratulations!</span> Your recipe page is now live. Please <a target="_blank" href="https://' . $domain . '">click here</a> to visit the page.';
                        $log->created_at = now()->format('H:i:s');
                        $log->save();
                    }
                }
            } else {
                //  if page is exist then do nothing and create store log
                $log = new StoreLog();
                $log->user_id = $store->id;
                $log->name = "Recipe Link";
                $log->value = 'This page already <span class="text-info">exists</span>! Please<a target="_blank" href="https://' . $domain . '"> click here</a> to visit the page.';
                $log->created_at = now()->format('H:i:s');
                $log->save();
            }

        } else {
            // File not found

            $log = new StoreLog();
            $log->user_id = $store->id;
            $log->name = "Recipe Page";
            $log->value = `A <span class="text-danger">Sorry!</span> The page you're trying to access is not exist. Please try again later!`;
            $log->created_at = now()->format('H:i:s');
            $log->save();
        }
        return redirect()->route('dashboard');
    }
}