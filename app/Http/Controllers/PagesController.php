<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function contactSupportPage(Request $request)
    {

        return view('pages.contact-support');
    }

    public function managePlanPage(Request $request)
    {

        return view('pages.manage-plan');
    }

    public function manageUserSettings(Request $request)
    {

        return view('pages.manage-setting');
    }

    public function layoutConfigurationSettings(Request $request){
        return view('pages.layout-config');
    }
}