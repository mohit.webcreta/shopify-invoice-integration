<?php

namespace App\Http\Controllers;

use App\Models\Choice;
use App\Models\Images;
use App\Models\Recipe;
use App\Models\RecipeProduct;
use App\Models\StoreLog;
use App\Models\Timing;
use Illuminate\Http\Request;
use Auth;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store = Auth::user();
        $items = Recipe::where('user_id', $store->id)->get();

        return view('master.recipes.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $store = Auth::user();
        $choices = Choice::where('user_id', $store->id)->where('status', 'published')->orderBy('order', 'asc')->get();

        return view('master.recipes.create', compact('choices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $recipe = Recipe::create($request->all());
        $products = json_decode($request->product_id, true);

        if ($products) {
            foreach ($products as $pr) {
                RecipeProduct::create([
                    'recipe_id' => $recipe->id,
                    'product_id' => $pr['value']
                ]);
            }
        }
        // check image is available or not
        if ($request->avatar) {
            $avatar = $request->avatar;
            $fileName = uniqid();
            $fileExtension = $avatar->getClientOriginalExtension();
            $fileImg = $fileName . "." . $fileExtension;
            $filePath = 'images/recipes/' . $recipe->id . '/';
            $fileURL = 'images/recipes/' . $recipe->id . '/' . $fileName . '.' . $fileExtension;
            $avatar->move($filePath, $fileImg);

            // store data
            $imageGallery = new Images();
            $imageGallery->name = "$fileName";
            $imageGallery->type = "$fileExtension";
            $imageGallery->url = "$fileURL";
            $imageGallery->save();
            $imageGallery->recipes()->attach($recipe->id);
        }

        if ($request->choices !== null) {
            foreach ($request->choices as $key => $choice_option) {
                foreach ($choice_option as $option) {
                    $recipe->choices()->attach($key, ['option_id' => $option]);
                }
            }
        }

        // insert log into store
        $log = new StoreLog();
        $log->user_id = $recipe->user_id;
        $log->name = "Recipe";
        $log->value = 'A <span class="text-success">new</span> recipe named <span class="text-info">' . $recipe->name . '</span> has been successfully added to our collection.';
        $log->created_at = now()->format('H:i:s');
        $log->save();

        return redirect()->route('recipes.index')->with('success', 'Your data has been saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        //
        $store = Auth::user();
        $choices = Choice::where('user_id', $store->id)->where('status', 'published')->orderBy('order', 'asc')->get();

        return view('master.recipes.edit', compact('recipe', 'choices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $recipe = Recipe::findOrFail($id);
        $recipe->update($request->all());
        $products = json_decode($request->product_id, true);

        if ($products) {
            $recipe->products()->where('recipe_id', $recipe->id)->delete();

            foreach ($products as $pr) {

                RecipeProduct::create([
                    'recipe_id' => $recipe->id,
                    'product_id' => $pr['value']
                ]);
            }
        }

        // check image is available or not
        if ($request->avatar) {
            $avatar = $request->avatar;
            $fileName = uniqid();
            $fileExtension = $avatar->getClientOriginalExtension();
            $fileImg = $fileName . "." . $fileExtension;
            $filePath = 'images/recipes/' . $recipe->id . '/';
            $fileURL = 'images/recipes/' . $recipe->id . '/' . $fileName . '.' . $fileExtension;
            $avatar->move($filePath, $fileImg);

            // store data
            $imageGallery = new Images();
            $imageGallery->name = "$fileName";
            $imageGallery->type = "$fileExtension";
            $imageGallery->url = "$fileURL";
            $imageGallery->save();
            $recipe->images()->detach();
            $imageGallery->recipes()->attach($recipe->id);
        }


        $recipe->choices()->detach();

        if ($request->choices !== null) {
            foreach ($request->choices as $key => $choice_option) {
                foreach ($choice_option as $option) {
                    $recipe->choices()->attach($key, ['option_id' => $option]);
                }
            }
        }

        // insert log into store
        $log = new StoreLog();
        $log->user_id = $recipe->user_id;
        $log->name = "Recipe";
        $log->value = 'An <span class="text-primary">existing</span> recipe named <span class="text-info">' . $recipe->name . '</span> has been successfully <span class="text-success">updated</span> to our collection.';
        $log->created_at = now()->format('H:i:s');
        $log->save();

        return redirect()->route('recipes.index')->with('success', 'Your data has been saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $recipe = Recipe::findOrFail($id);
        $recipe->choices()->detach();
        $recipe->images()->detach();
        $recipe->delete();

        // insert log into store
        $log = new StoreLog();
        $log->user_id = $recipe->user_id;
        $log->name = "Recipe";
        $log->value = 'A recipe named <span class="text-info">' . $recipe->name . '</span> has been successfully <span class="text-danger">deleted</span> from our collection.';
        $log->created_at = now()->format('H:i:s');
        $log->save();

        return redirect()->route('recipes.index')->with('success', 'Your data has been deleted successfully');
    }
}