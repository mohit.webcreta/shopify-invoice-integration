<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChoiceOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'choice_name'                   => 'required',
            'choice_order'                  => 'required|min:0',
        ];
    }

    public function messages()
    {
        return [
            'choice_options.*.option_name' => 'The option name field is required.',
            'choice_options.*.option_order' => 'The option order field is required.',
        ];
    }
}
