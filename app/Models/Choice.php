<?php

namespace App\Models;

use App\Enums\BaseStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    use HasFactory;

    protected $table = 'choices';

    protected $fillable = [
        'name',
        'user_id',
        'order',
        'status',
    ];


    /**
     * The status has multiple options.
     */
    protected $casts = [
        'status' => BaseStatusEnum::class
    ];

    /**
     * The options that belong to the options.
     */
    public function options()
    {
        return $this->belongsToMany(Option::class, 'choice_option');
    }

    /**
     * The 3 way pivot table that belong to the many choices.
     */
    public function recipes()
    {
        return $this->belongsToMany(
            Recipe::class,
            'choice_option_recipe'
        );
    }
}
