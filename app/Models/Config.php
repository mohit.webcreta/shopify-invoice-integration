<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $table = 'configs';
  
    public $fillable = [
      'title',
      'email',
      'mobile',
      'address',
      'logo',
      'favicon',
      'footer_bottom',
    ];
}
