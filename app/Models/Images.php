<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    use HasFactory;

    protected $table = 'images';

    protected $fillable = [
        'name',
        'type',
        'url',
    ];

    /**
     * The options that belong to the options.
     */
    public function recipes()
    {
        return $this->belongsToMany(Recipe::class, 'image_recipe');
    }
}
