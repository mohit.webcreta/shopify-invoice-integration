<?php

namespace App\Models;

use App\Enums\BaseStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $table = 'options';

    protected $fillable = [
        'name',
        'order',
        'status',
    ];

    protected $casts = [
        'status' => BaseStatusEnum::class
    ];

    /**
     * The options that belong to the options.
     */
    public function choices()
    {
        return $this->belongsToMany(Choice::class, 'choice_option');
    }

    /**
     * The 3 way pivot table that belong to the many choices.
     */
    public function recipes()
    {
        return $this->belongsToMany(
            Recipe::class,
            'choice_option_recipe'
        );
    }

    public function choice()
    {
        return $this->belongsToMany(Choice::class, 'choice_option_recipe', 'option_id', 'choice_id');
    }
}
