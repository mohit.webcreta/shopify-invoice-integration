<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PlanConfig;

class Plan extends Model
{
  use HasFactory;

  protected $table = 'plans';

  public $fillable = [
    'type',
    'name',
    'price',
    'interval',
    'capped_amount',
    'terms',
    'trial_days',
    'test',
    'on_install',
  ];

  // Define the relationship to User model through Subscription
  public function users()
  {
    return $this->belongsToMany(User::class, 'subscriptions', 'plan_id', 'user_id');
  }

  // Define the relationship to Subscription model
  public function subscriptions()
  {
    return $this->hasMany(Subscription::class, 'plan_id');
  }

  public function plan_config()
  {
    return $this->hasOne(PlanConfig::class, 'plan_id');
  }

  public function planConfig()
    {
        return $this->hasOne(PlanConfig::class, 'plan_id');
    }
}