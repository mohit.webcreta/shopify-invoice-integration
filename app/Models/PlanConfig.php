<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanConfig extends Model
{
    use HasFactory;

    protected $table = 'plan_config';

    protected $fillable = [
        'plan_id',
        'number_recipe_allow',
        'number_choice_allow',
        'number_list_layout_allow',
        'number_view_layout_allow'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }
}