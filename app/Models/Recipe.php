<?php

namespace App\Models;

use App\Enums\BaseStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Recipe extends Model
{
    use HasFactory;

    protected $table = 'recipes';


    protected $fillable = [
        'name',
        'url',
        'user_id',
        'slug',
        'video_url',
        'desc',
        'cooking_desc',
        'ingredient_desc',
        'pt_hh',
        'pt_mm',
        'pt_ss',
        'ct_hh',
        'ct_mm',
        'ct_ss',
        'serving_number',
        'status',
    ];

    protected $casts = [
        'status' => BaseStatusEnum::class
    ];

    /**
     * The options that belong to the options.
     */
    public function images()
    {
        return $this->belongsToMany(Images::class, 'image_recipe');
    }



    /**
     * The 3 way pivot table that belong to the many choices.
     */
    public function choices()
    {
        return $this->belongsToMany(
            Choice::class,
            'choice_option_recipe'
        );
    }

    /**
     * The 3 way pivot table that belong to the many options.
     */
    public function options()
    {
        return $this->belongsToMany(
            Option::class,
            'choice_option_recipe'
        );
    }

    public function choice()
    {
        return $this->belongsToMany(Choice::class, 'choice_option_recipe', 'recipe_id', 'choice_id');
    }

    public function products(): HasMany
    {
        return $this->hasMany(RecipeProduct::class);
    }

    public function option()
    {
        return $this->belongsToMany(Option::class, 'choice_option_recipe', 'recipe_id', 'option_id')->withPivot('choice_id');
    }
}
