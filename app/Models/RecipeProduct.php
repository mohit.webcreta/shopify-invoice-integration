<?php

namespace App\Models;

use App\Enums\BaseStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecipeProduct extends Model
{
  use HasFactory;

  protected $table = 'recipe_product';

  protected $fillable = [
    'recipe_id',
    'product_id',
  ];

  public function recipes()
  {
    return $this->belongsTo('Recipes');
  }
}
