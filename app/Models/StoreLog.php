<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class StoreLog extends Model
{
    use HasFactory;

    protected $table = 'store_log';

    protected $fillable = [
        'user_id',
        'name',
        'value',
    ];

    public function user(): BelongsTo
    {
      return $this->belongsTo(User::class)->withDefault();
    }
}
