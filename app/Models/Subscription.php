<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Subscription extends Model
{
  use HasFactory;

  protected $table = 'subscriptions';

  public $fillable = [
    'user_id',
    'plan_id',
    'start_date',
    'end_date',
    'status',
  ];

  public function user(): BelongsTo
  {
    return $this->belongsTo(User::class)->withDefault();
  }

  public function plan(): BelongsTo
  {
    return $this->belongsTo(Plan::class)->withDefault();
  }
}
