<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Osiset\ShopifyApp\Contracts\ShopModel as IShopModel;
use Osiset\ShopifyApp\Traits\ShopModel;
use App\Models\Plan;
use App\Models\Recipe;
use App\Models\Choice;

class User extends Authenticatable implements IShopModel
{
    use Notifiable;
    use ShopModel;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    // Define the relationship to Plan model through Subscription
    public function plans()
    {
        return $this->belongsToMany(Plan::class, 'subscriptions', 'user_id', 'plan_id');
    }

    // Define the relationship to Subscription model
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'user_id');
    }

    public function recipes()
    {
        return $this->hasMany(Recipe::class, 'user_id');
    }

    public function choices()
    {
        return $this->hasMany(Choice::class, 'user_id');
    }
}