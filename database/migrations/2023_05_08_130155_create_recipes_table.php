<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
            $table->string('name', 90);
            $table->string('slug', 90);
            $table->string('url');
            $table->string('video_url');
            $table->longText('desc');
            $table->longText('cooking_desc');
            $table->longText('ingredient_desc');
            $table->string('status');
            $table->string('pt_hh', 2);
            $table->string('pt_mm', 2);
            $table->string('pt_ss', 2);
            $table->string('ct_hh', 2);
            $table->string('ct_mm', 2);
            $table->string('ct_ss', 2);
            $table->string('serving_number', 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
};
