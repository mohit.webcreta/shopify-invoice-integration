<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_config', function (Blueprint $table) {
            $table->id();
            $table->foreignId('plan_id')->nullable();
            $table->longText('number_recipe_allow')->nullable();
            $table->longText('number_choice_allow')->nullable();
            $table->longText('number_list_layout_allow')->nullable();
            $table->longText('number_view_layout_allow')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_config');
    }
};
