<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name'          => 'Webcreta Admin',
            'email'         => 'admin@webcreta.com',
            'role_type'     => 'superadmin',
            'password'      => bcrypt('webcreta@#7707'),
        ]);
    }
}
