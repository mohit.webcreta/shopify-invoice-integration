<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Config::create([
            'title'     => 'Webcreta',
            'email'     => 'support@gmail.com',
            'contact'   => '9876543210',
            'address'   => '121, Street 11, California, USA',
            'logo'      => 'assets/media/logos/sample-logo.png',
            'favicon'   => 'assets/media/logos/sample-favicon.png',
            'footer_bottom' => 'Copyrights&copy; Shopify Recipe App'
        ]);
    }
}
