<?php

namespace Database\Seeders;

use App\Models\PlanConfig as Config;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlanConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $configs = [
            ['plan_id' => '1', 'number_recipe_allow' => '3', 'number_choice_allow' => '3', 'number_list_layout_allow' => '1', 'number_view_layout_allow' => '1'],
            ['plan_id' => '2', 'number_recipe_allow' => '10', 'number_choice_allow' => '10', 'number_list_layout_allow' => '3', 'number_view_layout_allow' => '3'],
            ['plan_id' => '3', 'number_recipe_allow' => '50', 'number_choice_allow' => '50', 'number_list_layout_allow' => '5', 'number_view_layout_allow' => '5'],
        ];

        Config::insert($configs);
    }
}

// php artisan db:seed --class=PlansSeeder