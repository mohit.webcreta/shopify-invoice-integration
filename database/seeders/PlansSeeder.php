<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Plan::create([
            'type'          => 'RECURRING',
            'name'          => 'Basic',
            'price'         => '20.00',
            'interval'      => 'EVERY_30_DAYS',
            'capped_amount' => '20',
            'terms'         => 'This is basic plan',
            'trial_days'    =>  1,
            'test'          => '0',
            'on_install'    => 1,
        ]);

        Plan::create([
            'type'          => 'RECURRING',
            'name'          => 'Standard',
            'price'         => '50.00',
            'interval'      => 'EVERY_30_DAYS',
            'capped_amount' => '20',
            'terms'         => 'This is standard plan',
            'trial_days'    =>  1,
            'test'          => '0',
            'on_install'    => 1,
        ]);

        Plan::create([
            'type'          => 'RECURRING',
            'name'          => 'Premium',
            'price'         => '70.00',
            'interval'      => 'EVERY_30_DAYS',
            'capped_amount' => '20',
            'terms'         => 'This is premium plan',
            'trial_days'    =>  1,
            'test'          => '0',
            'on_install'    => 1,
        ]);
    }
}

// php artisan db:seed --class=PlansSeeder