<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <meta charset="UTF-8">
    <title>Recipe App - 403 Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="{{ asset($config->favicon) }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/wc-custom.css') }}" rel="stylesheet" type="text/css" />

    <script src="https://kit.fontawesome.com/a4f8ae2a61.js" crossorigin="anonymous"></script>

</head>

<body>
    <div class="d-flex justify-content-center align-items-center mt-20">
        <div class="row justify-content-center text-center">
            <div class="col-sm-12">
                <img src="https://cdn3d.iconscout.com/3d/premium/thumb/403-forbidden-error-6194335-5073043.png?f=webp"
                    width="300px" />
            </div>
            <div class="col-sm-12">
                <div class="text-danger fw-bolder" style="font-size: 40px">Access Denied !</div>
                <p class="fs-3">The page you're trying to access has restricted access. <br> Please refer to your
                    system administrator</p>
                @if (Auth::check())
                    <a href="{{ route('authenticate') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                        class="btn btn-danger">
                        Go Back
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @else
                    <a href="{{ route('authenticate') }}" class="btn btn-danger">Go Back</a>
                @endif
            </div>
        </div>
    </div>
</body>

</html>
