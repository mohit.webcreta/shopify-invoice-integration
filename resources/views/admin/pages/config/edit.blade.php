@extends('layout.default-admin')

@section('head')
    <style>
        .image-input-placeholder {
            background-image: url('assets/media/logos/sample-logos.png');
        }

        [data-bs-theme="dark"] .image-input-placeholder {
            background-image: url('assets/media/logos/sample-logos.png');
        }
    </style>
@endsection

@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-black fw-bolder my-1 fs-3">Edit Config</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item text-white">
                        <a href="{{ route('home') }}" class="text-dark opacity-75 opacity-100-hover">Home</a>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        /
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75">Configs</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('wc-content')
    <form id="kt_ecommerce_add_category_form" class="form d-flex flex-column flex-lg-row"
        data-kt-redirect="{{ route('admin.config.update', $config->id) }}"
        action="{{ route('admin.config.update', $config->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 me-lg-10 mb-7">
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Site Configuration</h2>
                    </div>
                </div>

                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-sm-6 px-2 pb-4 pt-2">
                            <div class="row mb-6">
                                <label class="col-lg-12 col-form-label fw-semibold fs-6">Logo</label>
                                <div class="col-lg-12">
                                    <div class="image-input image-input-outline" data-kt-image-input="true"
                                        style="background: var(--bs-gray-300)">
                                        <div class="image-input-wrapper w-125px h-125px"
                                            style="background-image: {{ isset($config) && $config->logo ? 'url(' . asset($config->logo) . ')' : 'url(' . asset('assets/media/logos/sample-logo.png') . ')' }};">
                                        </div>
                                        <label
                                            class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                            data-kt-image-input-action="change" data-bs-toggle="tooltip"
                                            aria-label="Change logo" data-bs-original-title="Change logo"
                                            data-kt-initialized="1">
                                            <i class="fa-solid fa-pen-to-square fs-7"></i>
                                            <input type="file" name="logo" accept=".png, .jpg, .jpeg">
                                            <input type="hidden" name="custom_delete_logo">
                                        </label>
                                    </div>
                                    <div class="form-text">Allowed file types: png, jpg, jpeg.</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 px-2 pb-4 pt-2">
                            <div class="row mb-6">
                                <label class="col-lg-12 col-form-label fw-semibold fs-6">Favicon</label>
                                <div class="col-lg-12">
                                    <div class="image-input image-input-outline" data-kt-image-input="true"
                                        style="background: var(--bs-gray-300)">
                                        <div class="image-input-wrapper w-125px h-125px"
                                            style="background-image: {{ isset($config) && $config->favicon ? 'url(' . asset($config->favicon) . ')' : 'url(' . asset('assets/media/logos/sample-favicon.png') . ')' }};">
                                        </div>
                                        <label
                                            class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                            data-kt-image-input-action="change" data-bs-toggle="tooltip"
                                            aria-label="Change favicon" data-bs-original-title="Change favicon"
                                            data-kt-initialized="1">
                                            <i class="fa-solid fa-pen-to-square fs-7"></i>
                                            <input type="file" name="favicon" accept=".png, .jpg, .jpeg">
                                            <input type="hidden" name="custom_delete_favicon">
                                        </label>
                                    </div>
                                    <div class="form-text">Allowed file types: png, jpg, jpeg.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Site Title</label>
                        <input type="text" name="title" class="form-control mb-2" placeholder="Site Title"
                            value="{{ optional($config)->title }}">
                        @error('title')
                            <div class="fs-7 text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Site Email</label>
                        <input type="email" name="email" class="form-control mb-2" placeholder="Site Email"
                            min="0" value="{{ optional($config)->email }}">
                        @error('email')
                            <div class="fs-7 text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Site Contact</label>
                        <input type="number" name="contact" class="form-control mb-2" placeholder="Site Contact"
                            min="0" value="{{ optional($config)->contact }}">
                        @error('contact')
                            <div class="fs-7 text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class=" form-label">Site Address</label>
                        <input type="text" name="address" class="form-control mb-2" placeholder="Site Address"
                            min="0" value="{{ optional($config)->address }}">
                        @error('address')
                            <div class="fs-7 text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class=" form-label">Footer Bottom</label>
                        <textarea class="form-control" name="footer_bottom">{{$config->footer_bottom}}</textarea>
                        @error('footer_bottom')
                            <div class="fs-7 text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                    <span class="indicator-label">
                        Save
                    </span>
                </button>
            </div>
        </div>
    </form>
@endsection
