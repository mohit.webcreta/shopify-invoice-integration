@extends('layout.default-admin')


@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-black fw-bolder my-1 fs-3">Dashboard</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item text-white">
                        <a href="{{ route('home') }}" class="text-dark opacity-75 opacity-100-hover">Home</a>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        <span class="bullet bg-gray-700 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('wc-content')
    <div class="row g-5 g-xl-8">
        <div class="col-xl-4">
            <div class="row mb-5 mb-xl-8 g-5 g-xl-8">

                <div class="col-6">
                    <div class="card card-stretch">
                        <a href=""
                            class="btn btn-flex btn-text-gray-800 btn-icon-gray-400 btn-active-color-primary bg-body flex-column justfiy-content-start align-items-start text-start w-100 p-10">
                            <span class="fs-1">{{ $count['stores'] }}</span>
                            <span class="fs-5 fw-bold">
                                Installation</span>
                        </a>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card card-stretch">
                        <a href=""
                            class="btn btn-flex btn-text-gray-800 btn-icon-gray-400 btn-active-color-primary bg-body flex-column justfiy-content-start align-items-start text-start w-100 p-10">
                            <span class="fs-1">{{ $count['plans'] }}</span>
                            <span class="fs-5 fw-bold">
                                Active Plans </span>
                        </a>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card card-stretch">
                        <a href=""
                            class="btn btn-flex btn-text-gray-800 btn-icon-gray-400 btn-active-color-primary bg-body flex-column justfiy-content-start align-items-start text-start w-100 p-10">
                            <span class="fs-1">{{ $count['recipes'] }}</span>
                            <span class="fs-5 fw-bold">
                                Recipes Listed</span>
                        </a>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card card-stretch">
                        <a href=""
                            class="btn btn-flex btn-text-gray-800 btn-icon-gray-400 btn-active-color-primary bg-body flex-column justfiy-content-start align-items-start text-start w-100 p-10">
                            <span class="fs-1">{{ $count['choices'] }}</span>
                            <span class="fs-5 fw-bold">
                                Choices Listed</span>
                        </a>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card card-stretch">
                        <a href=""
                            class="btn btn-flex btn-text-gray-800 btn-icon-gray-400 btn-active-color-primary bg-body flex-column justfiy-content-start align-items-start text-start w-100 p-10">
                            <span class="fs-1">{{ $count['products'] }}</span>
                            <span class="fs-5">
                                Products Added</span>
                        </a>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card card-stretch">
                        <a href=""
                            class="btn btn-flex btn-text-gray-800 btn-icon-gray-400 btn-active-color-primary bg-body flex-column justfiy-content-start align-items-start text-start w-100 p-10">
                            <span class="fs-1">{{ $count['logs'] }}</span>
                            <span class="fs-5">
                                Logs Created</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 ps-xl-12">
            <div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px bg-body mb-5 mb-xl-8"
                style="background-position: 100% 20px;background-size: 300px auto;background-image:url('https://i.pinimg.com/originals/2a/22/73/2a22737c829158537637231d0ddb3ab8.jpg')"
                dir="ltr">
                <div class="card-body d-flex flex-column justify-content-center ps-lg-12">
                    <h3 class="text-dark fs-2qx fw-bold mb-7">
                        We are working <br>
                        to boost lovely mood
                    </h3>

                    <div class="m-0">
                        <a href="https://www.shopify.com/login" class="btn btn-dark fw-semibold px-6 py-3"
                            data-bs-toggle="modal">Install app</a>
                    </div>
                </div>
            </div>
            <div class="row g-5 g-xl-8 mb-5 mb-xl-8">
                <div class="col-xl-6">
                    <div class="card card-flush h-xl-100 mb-xl-8">
                        <div class="card-header pt-5">
                            <h3 class="card-title fw-bold text-dark">Plan Details</h3>
                        </div>

                        <div class="card-body d-flex justify-content-between flex-column pt-0">
                            <div class="m-0" id="kt_charts_widget_45" data-kt-chart-color="dark"
                                style="height: 90px; min-height: 90px;">
                                <div id="apexchartstz1nve8f"
                                    class="apexcharts-canvas apexchartstz1nve8f apexcharts-theme-light"
                                    style="width: 331px; height: 90px;"><svg id="SvgjsSvg1056" width="331"
                                        height="90" xmlns="http://www.w3.org/2000/svg" version="1.1"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev"
                                        class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                        style="background: transparent;">
                                        <foreignObject x="0" y="0" width="331" height="90">
                                            <div class="apexcharts-legend" xmlns="http://www.w3.org/1999/xhtml"
                                                style="max-height: 45px;"></div>
                                        </foreignObject>
                                        <g id="SvgjsG1100" class="apexcharts-yaxis" rel="0"
                                            transform="translate(-18, 0)">
                                        </g>
                                        <g id="SvgjsG1058" class="apexcharts-inner apexcharts-graphical"
                                            transform="translate(0, 0)">
                                            <defs id="SvgjsDefs1057">
                                                <clipPath id="gridRectMasktz1nve8f">
                                                    <rect id="SvgjsRect1060" width="338" height="93"
                                                        x="-3.5" y="-1.5" rx="0" ry="0"
                                                        opacity="1" stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                                <clipPath id="forecastMasktz1nve8f"></clipPath>
                                                <clipPath id="nonForecastMasktz1nve8f"></clipPath>
                                                <clipPath id="gridRectMarkerMasktz1nve8f">
                                                    <rect id="SvgjsRect1061" width="335" height="94"
                                                        x="-2" y="-2" rx="0" ry="0"
                                                        opacity="1" stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                                <linearGradient id="SvgjsLinearGradient1066" x1="0"
                                                    y1="0" x2="0" y2="1">
                                                    <stop id="SvgjsStop1067" stop-opacity="0.3"
                                                        stop-color="rgba(7,20,55,0.3)" offset="0.15"></stop>
                                                    <stop id="SvgjsStop1068" stop-opacity="0.2"
                                                        stop-color="rgba(255,255,255,0.2)" offset="1.2"></stop>
                                                    <stop id="SvgjsStop1069" stop-opacity="0.2"
                                                        stop-color="rgba(255,255,255,0.2)" offset="1"></stop>
                                                </linearGradient>
                                            </defs>
                                            <g id="SvgjsG1072" class="apexcharts-grid">
                                                <g id="SvgjsG1073" class="apexcharts-gridlines-horizontal"
                                                    style="display: none;">
                                                    <line id="SvgjsLine1076" x1="0" y1="0"
                                                        x2="331" y2="0" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1077" x1="0" y1="9"
                                                        x2="331" y2="9" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1078" x1="0" y1="18"
                                                        x2="331" y2="18" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1079" x1="0" y1="27"
                                                        x2="331" y2="27" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1080" x1="0" y1="36"
                                                        x2="331" y2="36" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1081" x1="0" y1="45"
                                                        x2="331" y2="45" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1082" x1="0" y1="54"
                                                        x2="331" y2="54" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1083" x1="0" y1="63"
                                                        x2="331" y2="63" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1084" x1="0" y1="72"
                                                        x2="331" y2="72" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1085" x1="0" y1="81"
                                                        x2="331" y2="81" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1086" x1="0" y1="90"
                                                        x2="331" y2="90" stroke="#e0e0e0"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                </g>
                                                <g id="SvgjsG1074" class="apexcharts-gridlines-vertical"
                                                    style="display: none;"></g>
                                                <line id="SvgjsLine1088" x1="0" y1="90" x2="331"
                                                    y2="90" stroke="transparent" stroke-dasharray="0"
                                                    stroke-linecap="butt">
                                                </line>
                                                <line id="SvgjsLine1087" x1="0" y1="1" x2="0"
                                                    y2="90" stroke="transparent" stroke-dasharray="0"
                                                    stroke-linecap="butt">
                                                </line>
                                            </g>
                                            <g id="SvgjsG1062" class="apexcharts-area-series apexcharts-plot-series">
                                                <g id="SvgjsG1063" class="apexcharts-series" seriesName="Overview"
                                                    data:longestSeries="true" rel="1" data:realIndex="0">
                                                    <path id="SvgjsPath1070"
                                                        d="M 0 90 L 0 67.5C 19.30833333333333 67.5 35.858333333333334 67.5 55.166666666666664 67.5C 74.475 67.5 91.025 27 110.33333333333333 27C 129.64166666666665 27 146.19166666666666 27 165.49999999999997 27C 184.8083333333333 27 201.35833333333332 70.5 220.66666666666666 70.5C 239.975 70.5 256.525 70.5 275.8333333333333 70.5C 295.14166666666665 70.5 311.6916666666666 37.5 330.99999999999994 37.5C 330.99999999999994 37.5 330.99999999999994 37.5 330.99999999999994 90M 330.99999999999994 37.5z"
                                                        fill="url(#SvgjsLinearGradient1066)" fill-opacity="1"
                                                        stroke-opacity="1" stroke-linecap="butt" stroke-width="0"
                                                        stroke-dasharray="0" class="apexcharts-area" index="0"
                                                        clip-path="url(#gridRectMasktz1nve8f)"
                                                        pathTo="M 0 90 L 0 67.5C 19.30833333333333 67.5 35.858333333333334 67.5 55.166666666666664 67.5C 74.475 67.5 91.025 27 110.33333333333333 27C 129.64166666666665 27 146.19166666666666 27 165.49999999999997 27C 184.8083333333333 27 201.35833333333332 70.5 220.66666666666666 70.5C 239.975 70.5 256.525 70.5 275.8333333333333 70.5C 295.14166666666665 70.5 311.6916666666666 37.5 330.99999999999994 37.5C 330.99999999999994 37.5 330.99999999999994 37.5 330.99999999999994 90M 330.99999999999994 37.5z"
                                                        pathFrom="M -1 90 L -1 90 L 55.166666666666664 90 L 110.33333333333333 90 L 165.49999999999997 90 L 220.66666666666666 90 L 275.8333333333333 90 L 330.99999999999994 90">
                                                    </path>
                                                    <path id="SvgjsPath1071"
                                                        d="M 0 67.5C 19.30833333333333 67.5 35.858333333333334 67.5 55.166666666666664 67.5C 74.475 67.5 91.025 27 110.33333333333333 27C 129.64166666666665 27 146.19166666666666 27 165.49999999999997 27C 184.8083333333333 27 201.35833333333332 70.5 220.66666666666666 70.5C 239.975 70.5 256.525 70.5 275.8333333333333 70.5C 295.14166666666665 70.5 311.6916666666666 37.5 330.99999999999994 37.5"
                                                        fill="none" fill-opacity="1" stroke="#071437"
                                                        stroke-opacity="1" stroke-linecap="butt" stroke-width="3"
                                                        stroke-dasharray="0" class="apexcharts-area" index="0"
                                                        clip-path="url(#gridRectMasktz1nve8f)"
                                                        pathTo="M 0 67.5C 19.30833333333333 67.5 35.858333333333334 67.5 55.166666666666664 67.5C 74.475 67.5 91.025 27 110.33333333333333 27C 129.64166666666665 27 146.19166666666666 27 165.49999999999997 27C 184.8083333333333 27 201.35833333333332 70.5 220.66666666666666 70.5C 239.975 70.5 256.525 70.5 275.8333333333333 70.5C 295.14166666666665 70.5 311.6916666666666 37.5 330.99999999999994 37.5"
                                                        pathFrom="M -1 90 L -1 90 L 55.166666666666664 90 L 110.33333333333333 90 L 165.49999999999997 90 L 220.66666666666666 90 L 275.8333333333333 90 L 330.99999999999994 90"
                                                        fill-rule="evenodd"></path>
                                                    <g id="SvgjsG1064"
                                                        class="apexcharts-series-markers-wrap apexcharts-hidden-element-shown"
                                                        data:realIndex="0">
                                                        <g class="apexcharts-series-markers">
                                                            <circle id="SvgjsCircle1104" r="0" cx="0"
                                                                cy="0"
                                                                class="apexcharts-marker w18vc08x5 no-pointer-events"
                                                                stroke="#071437" fill="#071437" fill-opacity="1"
                                                                stroke-width="3" stroke-opacity="0.9"
                                                                default-marker-size="0"></circle>
                                                        </g>
                                                    </g>
                                                </g>
                                                <g id="SvgjsG1065" class="apexcharts-datalabels" data:realIndex="0"></g>
                                            </g>
                                            <g id="SvgjsG1075" class="apexcharts-grid-borders" style="display: none;">
                                            </g>
                                            <line id="SvgjsLine1089" x1="0" y1="0" x2="331"
                                                y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                stroke-linecap="butt" class="apexcharts-ycrosshairs"></line>
                                            <line id="SvgjsLine1090" x1="0" y1="0" x2="331"
                                                y2="0" stroke-dasharray="0" stroke-width="0"
                                                stroke-linecap="butt" class="apexcharts-ycrosshairs-hidden"></line>
                                            <g id="SvgjsG1091" class="apexcharts-xaxis" transform="translate(0, 0)">
                                                <g id="SvgjsG1092" class="apexcharts-xaxis-texts-g"
                                                    transform="translate(0, -4)"></g>
                                            </g>
                                            <g id="SvgjsG1101" class="apexcharts-yaxis-annotations"></g>
                                            <g id="SvgjsG1102" class="apexcharts-xaxis-annotations"></g>
                                            <g id="SvgjsG1103" class="apexcharts-point-annotations"></g>
                                        </g>
                                    </svg>
                                    <div class="apexcharts-tooltip apexcharts-theme-light">
                                        <div class="apexcharts-tooltip-title"
                                            style="font-family: inherit; font-size: 12px;"></div>
                                        <div class="apexcharts-tooltip-series-group" style="order: 1;"><span
                                                class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(7, 20, 55);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: inherit; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span></div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span></div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-light">
                                        <div class="apexcharts-xaxistooltip-text"
                                            style="font-family: inherit; font-size: 12px;"></div>
                                    </div>
                                    <div
                                        class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                        <div class="apexcharts-yaxistooltip-text"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="m-0">
                                @foreach ($plans as $plan)
                                    <div class="d-flex flex-stack mb-9">
                                        <div class="d-flex align-items-center me-2">
                                            <div class="symbol symbol-50px me-5">
                                                <div class="symbol-label bg-light">
                                                    <img src="/metronic8/demo34/assets/media/svg/brand-logos/plurk.svg"
                                                        alt="" class="h-50">
                                                </div>
                                            </div>

                                            <div>
                                                <a href="#"
                                                    class="fs-6 text-gray-800 text-hover-primary fw-bold">{{ $plan->name }}</a>
                                                <div class="fs-7 text-muted fw-semibold mt-1">
                                                    @if ($plan->name == 'Free')
                                                        Free to use and access @else{{ $plan->trial_days }} day free trial
                                                        access
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="badge badge-light-dark badge-lg fw-bold p-2 text-gray-600">
                                            {{ $plan->price == '0.00' ? 'Free' : '$' . $plan->price }}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6">

                    <div class="card card-flush h-xl-100 mb-xl-8">
                        <div class="card-header py-7">
                            <div class="m-0">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="fs-3 fw-bold text-gray-800 me-2 lh-1 ls-n2">Store details</span>
                                </div>

                                <span class="fs-6 fw-semibold text-gray-400">Stores active plan details</span>
                            </div>
                        </div>

                        <div class="card-body pt-0">
                            <div class="mb-0">
                                @foreach ($stores as $key => $store)
                                    @if ($key > 0)
                                        <div class="separator separator-dashed my-3"></div>
                                    @endif
                                    <div class="d-flex flex-stack">
                                        <div class="d-flex align-items-center me-5">

                                            <div class="me-5">
                                                <a href="#"
                                                    class="text-gray-800 fw-bold text-hover-primary fs-6">{{ count($store->subscriptions) > 0 ? $store->subscriptions[0]->plan->name . ' plan' : 'No plan selected' }}</a>

                                                <span
                                                    class="text-gray-700 fw-semibold fs-7 d-block text-start ps-0"> {{ $store->name }}</span>
                                            </div>
                                        </div>
                                        @if (count($store->subscriptions) > 0)
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex flex-center">
                                                    <span class="badge badge-light-danger fs-8">
                                                        <i class="ki-outline ki-arrow-down fs-6 text-danger ms-n1"></i>
                                                        {{ date('d-m-y', strtotime($store->subscriptions[0]->end_date)) }}
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
