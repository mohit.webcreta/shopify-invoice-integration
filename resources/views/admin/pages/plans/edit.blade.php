@extends('layout.default-admin')

@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-black fw-bolder my-1 fs-3">Edit Plan "{{ $plan->name }}"</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item">
                        <a href="{{ route('home') }}" class="text-dark opacity-75 opacity-100-hover">Home</a>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        /
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.plans.index') }}" class="text-dark opacity-75 opacity-100-hover">Plans</a>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        /
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75">Edit</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('wc-content')
    <form id="kt_ecommerce_add_category_form" class="form d-flex flex-column flex-lg-row"
        data-kt-redirect="{{ route('admin.plans.update', $plan->id) }}"
        action="{{ route('admin.plans.update', $plan->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 me-lg-10 mb-7">
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Plan Details</h2>
                    </div>
                </div>

                <div class="card-body pt-5">
                    <div class="mb-5 row">
                        <div class="col-md-8 col-sm-12 mb-5">
                            <label class="required form-label">Plan Name</label>
                            <input type="text" name="name" class="form-control mb-2" placeholder="Plan Name"
                                value="{{ optional($plan)->name }}">
                            @error('name')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4 col-sm-12 mb-5">
                            <label class="required form-label">Plan Type</label>
                            <input type="text" name="type" class="form-control mb-2" placeholder="Plan Type"
                                value="{{ optional($plan)->type }}">
                            @error('type')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4 col-sm-12 mb-5">
                            <label class="required form-label">Plan Inerval</label>
                            <input type="text" name="interval" class="form-control mb-2" placeholder="Plan Inerval"
                                value="{{ optional($plan)->interval }}">
                            @error('interval')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4 col-sm-12 mb-5">
                            <label class="required form-label">Plan Terms</label>
                            <input type="text" name="terms" class="form-control mb-2" placeholder="Plan Terms"
                                value="{{ optional($plan)->terms }}">
                            @error('terms')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4 col-sm-12 mb-5">
                            <label class="required form-label">No. Of Trial Days</label>
                            <input type="text" name="trial_days" class="form-control mb-2"
                                placeholder="No. Of Trial Days" value="{{ optional($plan)->trial_days }}">
                            @error('trial_days')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4 col-sm-12 mb-5">
                            <label class="required form-label">Price</label>
                            <input type="number" step="0.01" min="0" name="price" class="form-control mb-2"
                                placeholder="Price" value="{{ optional($plan)->price }}">
                            @error('price')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-sm-12 mt-5">
                            <div class="form-check form-switch form-check-custom form-check-solid">
                                <input class="form-check-input" type="checkbox" name="test"
                                    @if ($plan->test == 1) checked @endif id="testEnable" />
                                <label class="form-check-label" for="testEnable">
                                    Test Enable
                                </label>
                            </div>
                            @error('test')
                                <div class="fs-7 text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Plan Configuration Details</h2>
                    </div>
                </div>

                <div class="card-body pt-5">
                    <div class="mb-5 row">
                        <div class="col-md-6 col-sm-12 mb-5">
                            <label class="required form-label">No. Of Recipes Allow</label>
                            <input type="number" min="0" name="number_recipe_allow" class="form-control mb-2" placeholder="No. Of Recipes Allow"
                                value="{{ $plan->plan_config->number_recipe_allow ?? 0 }}">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-5">
                            <label class="required form-label">No. Of Choices Allow</label>
                            <input type="number" min="0" name="number_choice_allow" class="form-control mb-2" placeholder="No. Of Choices Allow"
                                value="{{ $plan->plan_config->number_choice_allow ?? 0 }}">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-5">
                            <label class="required form-label">No. Of Listing Layout Show</label>
                            <input type="number" min="0" name="number_list_layout_allow" class="form-control mb-2" placeholder="No. Of Listing Layout Show"
                                value="{{ $plan->plan_config->number_list_layout_allow ?? 0 }}">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-5">
                            <label class="required form-label">No. Of View Layout Show</label>
                            <input type="number" min="0" name="number_view_layout_allow" class="form-control mb-2" placeholder="No. Of View Layout Show"
                                value="{{ $plan->plan_config->number_view_layout_allow ?? 0}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                    <span class="indicator-label">
                        Save
                    </span>
                </button>
            </div>
        </div>
    </form>
@endsection
