@extends('layout.default-admin')


@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-black fw-bolder my-1 fs-3">Plans List</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item text-white">
                        <a href="{{ route('home') }}" class="text-dark opacity-75 opacity-100-hover">Home</a>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        /
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75">Plans</li>
                </ul>
            </div>
        </div>
    </div>
@endsection


@section('wc-content')
    <div class="card card-flush">
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4"><span class="path1"></span><span
                            class="path2"></span></i> <input type="text" data-kt-ecommerce-category-filter="search"
                        class="form-control form-control-solid w-250px ps-12" placeholder="Search Keyword">
                </div>
            </div>
        </div>

        <div class="card-body pt-0">

            <div id="kt_ecommerce_category_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                        id="kt_ecommerce_category_table">
                        <thead>
                            <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">

                                <th class="min-w-250px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="name: activate to sort column ascending">
                                    Plan Name</th>
                                <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="type: activate to sort column ascending">
                                    Plan Type</th>
                                <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="price: activate to sort column ascending">
                                    Price</th>
                                <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="interval: activate to sort column ascending">
                                    Interval</th>
                                <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1"
                                    aria-label="tiral_days: activate to sort column ascending">
                                    No. of Trial Days</th>
                                <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="test: activate to sort column ascending">
                                    Test Mode</th>
                            </tr>
                        </thead>
                        <tbody class="fw-semibold text-gray-600">
                            @foreach ($plans as $item)
                                <tr>
                                    <td>
                                        <div class="d-flex">
                                            <div class="ms-5">
                                                <a href="{{ route('admin.plans.edit', $item->id) }}"
                                                    class="text-gray-800 text-hover-primary fs-5 fw-bold mb-1"
                                                    data-kt-ecommerce-category-filter="category_name">{{ $item->name }}</a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $item->type }}
                                    </td>
                                    <td>
                                        {{ $item->price }}
                                    </td>
                                    <td>
                                        <span class="badge badge-dark">{{ $item->interval }}</span>
                                    </td>
                                    <td>
                                        {{ $item->trial_days }}
                                    </td>
                                    <td>
                                        @if ($item->test)
                                            <span class="badge badge-success">ON</span>
                                        @else
                                            <span class="badge badge-info">OFF</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
