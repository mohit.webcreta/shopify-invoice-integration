@extends('layout.default-admin')


@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-black fw-bolder my-1 fs-3">Store Log</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item text-white">
                        <a href="{{ route('home') }}" class="text-dark opacity-75 opacity-100-hover">Home</a>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        <span class="bullet bg-gray-700 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75">Stores</li>
                    <li class="breadcrumb-item text-dark opacity-75 fs-8">
                        <span class="bullet bg-gray-700 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-dark opacity-75">Logs</li>
                </ul>
            </div>
        </div>
    </div>
@endsection


@section('wc-content')
    <div class="card card-flush">
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4"><span class="path1"></span><span
                            class="path2"></span></i> <input type="text" data-kt-ecommerce-category-filter="search"
                        class="form-control form-control-solid w-250px ps-12" placeholder="Search Keyword">
                </div>
            </div>
        </div>

        <div class="card-body pt-0">

            <div id="kt_ecommerce_category_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                        id="kt_ecommerce_category_table">
                        <thead>
                            <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                <th class="min-w-50px" tabindex="0" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="name: activate to sort column ascending">
                                    No.</th>
                                <th class="min-w-150px" tabindex="2" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="plan: activate to sort column ascending">
                                    Module Name</th>
                                <th class="min-w-100px sorting" tabindex="3" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1"
                                    aria-label="plan-start: activate to sort column ascending">
                                    Description</th>
                                <th class="min-w-150px" tabindex="4" aria-controls="kt_ecommerce_category_table"
                                    rowspan="1" colspan="1" aria-label="plan-end: activate to sort column ascending">
                                    Date</th>
                            </tr>
                        </thead>
                        <tbody class="fw-semibold text-gray-600">
                            @foreach ($logs as $key => $item)
                                <tr>
                                    <td class="text-gray-700 fw-bold">{{ $logTotal-- }}</td>
                                    <td class="text-gray-700 fw-bold"><a href="#">{{ $item->name }}</a></td>
                                    <td class="text-gray-700 fw-bold">{!! $item->value !!}</td>
                                    <td class="text-gray-700 fw-bold">
                                        {{ $item->created_at->format('d, F, Y \a\t H:i') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
