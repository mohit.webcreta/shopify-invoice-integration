@extends('layout.auth-admin')


@section('wc-content')
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                <div class="d-flex flex-center flex-column flex-lg-row-fluid">
                    <div class="w-lg-500px p-10">
                        @if (session('error'))
                            <div class="notice d-flex bg-light-danger rounded border-danger border border-dashed mb-9 p-6">

                                <div class="d-flex flex-stack flex-grow-1">
                                    <div class="fw-semibold">
                                        <div class="fs-6 text-gray-700">{{ session('error') }}
                                        </div>
                                    </div>

                                </div>

                            </div>
                        @endif

                        <form class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework" method="POST"
                            novalidate="novalidate" id="kt_sign_in_form" action="{{ route('login') }}">
                            @csrf
                            <div class="text-center mb-11">
                                <h1 class="text-dark fw-bolder mb-3">Sign In</h1>
                                <div class="text-gray-500 fw-semibold fs-6">Login to access your account</div>
                            </div>
                            {{-- <div class="row g-3 mb-9">
                                <div class="col-md-6">
                                    <a href="#"
                                        class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                                        <img alt="Logo"
                                            src="/metronic8/demo6/assets/media/svg/brand-logos/google-icon.svg"
                                            class="h-15px me-3">Sign in with Google</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#"
                                        class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                                        <img alt="Logo"
                                            src="/metronic8/demo6/assets/media/svg/brand-logos/apple-black.svg"
                                            class="theme-light-show h-15px me-3">
                                        <img alt="Logo"
                                            src="/metronic8/demo6/assets/media/svg/brand-logos/apple-black-dark.svg"
                                            class="theme-dark-show h-15px me-3">Sign in with Apple</a>
                                </div>
                            </div> --}}
                            <div class="fv-row mb-8 fv-plugins-icon-container">
                                <input type="text" placeholder="Email" name="email" autocomplete="off"
                                    class="form-control bg-transparent">
                                @error('email')
                                    <div data-field="email" data-validator="notEmpty" class="text-danger mt-1">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="fv-row mb-3 fv-plugins-icon-container">
                                <input type="password" placeholder="Password" name="password" autocomplete="off"
                                    class="form-control bg-transparent">
                                @error('password')
                                    <div data-field="password" data-validator="notEmpty" class="text-danger mt-1">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-8">
                                <div></div>
                                <a href="{{ route('password.request') }}" class="link-primary">Forgot Password ?</a>
                            </div>
                            <div class="d-grid mb-10">
                                <button type="submit" id="kt_sign_in_submit" class="btn btn-info">
                                    <span class="indicator-label">Sign In</span>
                                    <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                            </div>
                            <div class="text-gray-500 text-center fw-semibold fs-6">Not a Member yet?
                                <a href="{{ route('register') }}" class="link-info">Sign up</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2 bg-auth">
                <div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
                    <img class="d-none d-lg-block mx-auto w-100px w-md-50 w-xl-200px mb-0 mb-lg-20"
                        src="{{asset($config->logo)}}" alt="">
                    <h6 class="mb-0 mb-lg-12 text-white text-uppercase fs-2qx fw-bolder">
                        Welcome back to {{$config->title}}
                    </h6>
                    <h1 class="d-none d-lg-block text-white fs-2 fw-bolder text-light mb-7">Trusted by over 75,000 stores
                        worldwide</h1>
                    <div class="d-none d-lg-block text-white fs-5 w-75 text-center ">your ultimate solution for seamlessly combining the world of culinary delights with the power of e-commerce! This innovative project is designed to transform your Shopify store into a culinary hub, allowing you to create and share recipes while monetizing your passion for cooking through a subscription-based model.
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- include footer -->
    <div class="footer py-4 d-flex flex-lg-column position-fixed w-100 bottom-0" id="wc_footer">
        <div class="container d-flex flex-column flex-md-row flex-stack">
            <div class="text-dark order-2 order-md-1">
                <span class="text-gray-600 fw-bold me-1">All rikghts reserved by Webcreta</span>
            </div>
            <ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
                
                <li class="menu-item">
                    <a href="{{ route('home') }}" target="_blank" class="menu-link px-2 text-white">
                        {{ date('Y') }} Copyright&copy; Webcreta Technologies Pvt. Ltd..</a>
                </li>
            </ul>
        </div>
    </div>

@endsection
