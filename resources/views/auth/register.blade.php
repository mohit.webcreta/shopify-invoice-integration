@extends('layout.auth-admin')


@section('wc-content')
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                <div class="d-flex flex-center flex-column flex-lg-row-fluid">
                    <div class="w-lg-500px p-10">
                        @if (session('error'))
                            <div class="notice d-flex bg-light-danger rounded border-danger border border-dashed mb-9 p-6">

                                <div class="d-flex flex-stack flex-grow-1">
                                    <div class="fw-semibold">
                                        <div class="fs-6 text-gray-700">{{ session('error') }}
                                        </div>
                                    </div>

                                </div>

                            </div>
                        @endif

                        <form class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework" method="POST"
                            novalidate="novalidate" id="kt_sign_in_form" action="{{ route('register') }}">
                            @csrf
                            <div class="text-center mb-11">
                                <h1 class="text-dark fw-bolder mb-3">Sign Up</h1>
                                <div class="text-gray-500 fw-semibold fs-6">Regsiter with us for free</div>
                            </div>
                            <div class="fv-row mb-8 fv-plugins-icon-container">
                                <input type="text" placeholder="Name" name="name" autocomplete="off"
                                    class="form-control bg-transparent">
                                @error('name')
                                    <div data-field="name" data-validator="notEmpty" class="text-danger mt-1">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="fv-row mb-8 fv-plugins-icon-container">
                                <input type="text" placeholder="Email" name="email" autocomplete="off"
                                    class="form-control bg-transparent">
                                @error('email')
                                    <div data-field="email" data-validator="notEmpty" class="text-danger mt-1">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="fv-row mb-8 fv-plugins-icon-container">
                                <input type="password" placeholder="Password" name="password" autocomplete="off"
                                    class="form-control bg-transparent">
                                @error('password')
                                    <div data-field="password" data-validator="notEmpty" class="text-danger mt-1">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="fv-row mb-8 fv-plugins-icon-container">
                                <input type="password" placeholder="Confirm password" name="password_confirmation"
                                    autocomplete="off" class="form-control bg-transparent">
                                @error('password_confirmation')
                                    <div data-field="password_confirmation" data-validator="notEmpty" class="text-danger mt-1">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="d-grid mb-10">
                                <button type="submit" id="kt_sign_in_submit" class="btn btn-info">
                                    <span class="indicator-label">Sign Up</span>
                                    <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                            </div>
                            <div class="text-gray-500 text-center fw-semibold fs-6">Already have an Account?
                                <a href="{{ route('login') }}" class="link-info">Sign In</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2 bg-auth">
                <div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
                    <h6 class="mb-0 mb-lg-12 text-white text-uppercase">
                        Welcome back to Webcreta
                    </h6>
                    <img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-500px mb-10 mb-lg-20"
                        src="/metronic8/demo6/assets/media/misc/auth-screens.png" alt="">
                    <h1 class="d-none d-lg-block text-white fs-2qx fw-bolder text-center mb-7">Trusted by over 75,000 teams
                        worldwide</h1>
                    <div class="d-none d-lg-block text-white fs-base text-center">a platform for hiring that makes the
                        process 10X faster & more effective for remote-first businesses.<br>
                        a talent pool of more than 50,000 people who have been thoroughly screened<br> for their technical
                        expertise, language ability, and behavioural traits.
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- include footer -->
    <div class="footer py-4 d-flex flex-lg-column position-fixed w-100 bottom-0" id="wc_footer">
        <div class="container d-flex flex-column flex-md-row flex-stack">
            <div class="text-dark order-2 order-md-1">
                <span class="text-gray-600 fw-bold me-1">All rikghts reserved by Webcreta</span>
            </div>
            <ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
                {{-- <li class="menu-item">
                        <a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
                    </li>
                    <li class="menu-item">
                        <a href="{{ route('frontend.index') }}" target="_blank" class="menu-link px-2">Support</a>
                    </li>
                    <li class="menu-item">
                        <a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
                    </li> --}}
                <li class="menu-item">
                    <a href="{{ route('home') }}" target="_blank" class="menu-link px-2 text-white">
                        {{ date('Y') }} Copyright&copy;.</a>
                </li>
            </ul>
        </div>
    </div>
@endsection
