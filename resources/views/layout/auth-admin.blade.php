<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
    @yield('wc-head')
</head>

<body id="kt_body" style="background: #f1f2f4;" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">


    @yield('wc-content')

    @include('includes.script')

    @if (session('success'))
        @include('msg.success')
    @endif

    @if (session('error'))
        @include('msg.error')
    @endif
</body>

</html>
