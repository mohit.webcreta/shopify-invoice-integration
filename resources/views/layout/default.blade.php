<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
    @yield('wc-head')
</head>

<body id="kt_body" style="background: #f1f2f4;" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            <!--begin::Wrapper-->
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                @include('includes.header')
                @include('includes.toolbar')

                <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
                    <!--begin::Post-->
                    <div class="content flex-row-fluid" id="kt_content">
                        @yield('wc-content')
                    </div>
                </div>

                @include('includes.footer')

            </div>
        </div>
    </div>

    @include('includes.script')

    @if (session('success'))
        @include('msg.success')
    @endif

    @if (session('error'))
        @include('msg.error')
    @endif
</body>

</html>
