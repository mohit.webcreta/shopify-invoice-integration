@extends('layout.default')

@section('toolbar')
<div class="toolbar py-5 py-lg-15" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-3">
            <h1 class="d-flex text-black fw-bolder my-1 fs-3">New Recipe Choice</h1>
            <small>Add new choices and options details</small>
        </div>
        <div class="d-flex gap-4 gap-lg-13">

            <div class="d-flex flex-column">
                <a href="{{ route('choices.index') }}"
                    class="btn btn-outline btn-outline-dashed btn-outline-dark btn-active-light-dark btn-sm">
                    Back
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('wc-content')
<form id="kt_ecommerce_add_category_form" class="form d-flex flex-column flex-lg-row"
    data-kt-redirect="{{ route('choices.store') }}" action="{{ route('choices.store') }}" method="POST">
    @csrf
    <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 me-lg-10 mb-7">
        <div class="card card-flush py-4">
            <div class="card-header">
                <div class="card-title">
                    <h2>General</h2>
                </div>
            </div>

            <div class="card-body pt-0">
                <div class="mb-10 fv-row">
                    <label class="required form-label">Choice Name</label>
                    <input type="text" name="choice_name" class="form-control mb-2" placeholder="Choice name" value="" required>
                    <input type="hidden" name="user_id" class="form-control mb-2" value="{{Auth::user()->id}}">

                    @error('choice_name')
                    <div class="fs-7 text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-10 fv-row">
                    <label class="required form-label">Order</label>
                    <input type="number" name="choice_order" class="form-control mb-2" placeholder="Choice Order"
                        min="0" value="0" required>
                    @error('choice_order')
                    <div class="fs-7 text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="card card-flush py-4">
            <div class="card-header">
                <div class="card-title">
                    <h2>Choice Options</h2>
                </div>
            </div>
            <div class="card-body pt-0">
                <div id="kt_docs_repeater_basic">
                    <div class="form-group">
                        <div data-repeater-list="choice_options">
                            <div data-repeater-item>
                                <div class="form-group row my-3">
                                    <div class="col-md-6">
                                        <label class="form-label required">Name:</label>
                                        <input type="name" name="option_name" class="form-control mb-2"
                                            placeholder="Enter option name" required />
                                            <input type="hidden" min="0" name="option_order" class="form-control mb-2"
                                                placeholder="Enter option order" value="0" />
                                        @error('choice_options.*.option_name')
                                        <div class="fs-7 text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2">
                                        <a href="javascript:;" data-repeater-delete
                                            class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                            <i class="far fa-trash-alt"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-5">
                        <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                            <i class="fas fa-plus"></i>
                            Add
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <div class="me-3">
                <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                    data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select" name="status">
                    @foreach (\App\Enums\BaseStatusEnum::cases() as $status)
                    <option value="{{ $status->value }}">
                        {{ $status->name }}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="ml-1">
                <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                    <span class="indicator-label">
                        Save
                    </span>
                </button>
            </div>
        </div>
    </div>

</form>
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>


<script>
    $('#kt_docs_repeater_basic').repeater({
        initEmpty: false,

        defaultValues: {
            'text-input': 'foo'
        },

        show: function () {
            $(this).slideDown();
        },

        hide: function (deleteElement) {
            $(this).slideUp(deleteElement);
        }
    });
</script>
@endsection

{{-- <div class="card card-flush py-4">
    <div class="card-header">
        <div class="card-title">
            <h2>Thumbnail</h2>
        </div>
    </div>

    <div class="card-body text-center pt-0">
        <style>
            .image-input-placeholder {
                background-image: url('{{ asset(' images/blank-image.svg') }}');
            }
        </style>

        <div class="image-input image-input-empty image-input-outline image-input-placeholder mb-3"
            data-kt-image-input="true">
            <div class="image-input-wrapper w-150px h-150px"></div>

            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                <i class="fa-solid fa-pen"></i>

                <input type="file" name="avatar" accept=".png, .jpg, .jpeg">
                <input type="hidden" name="avatar_remove">
            </label>

            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                <i class="fa-solid fa-trash"></i>
            </span>

            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                <i class="fa-solid fa-trash"></i>
            </span>
        </div>

        <div class="text-muted fs-7">Set the category thumbnail image. Only *.png, *.jpg and *.jpeg image files
            are accepted</div>
    </div>
</div> --}}