@extends('layout.default')

@php
    // get auth data
    $shop = Auth::user();
    $subscription = App\Models\Subscription::where('user_id', '=', $shop->id)
        ->orderBy('id', 'desc')
        ->first();
    // get authenticated shop data
    $planConfig = $subscription->plan->plan_config;
    $totalChoices = count($shop->choices);
    $exceedChoice = $totalChoices == (int) $planConfig->number_choice_allow ? true : false;
@endphp

@section('toolbar')
<div class="toolbar py-5 py-lg-15" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-3">
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Recipe Choices</h1>
            <small>{{ $items->count() }} results found</small>
        </div>
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column">
               @if ($exceedChoice)
                        <button type="button" class="btn btn-secondary btn-sm" id="wc_choice_alert">
                            {{ __('Add New Choice') }}
                        </button>
                    @else
                        <a href="{{ route('choices.create') }}" class="btn btn-secondary btn-sm">
                            {{ __('Add New Choice') }}
                        </a>
                    @endif
            </div>
        </div>
    </div>
</div>
@endsection


@section('wc-content')
<div class="card card-flush">
    <div class="card-header align-items-center py-5 gap-2 gap-md-5">
        <div class="card-title">
            <div class="d-flex align-items-center position-relative my-1">
                <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4"><span class="path1"></span><span class="path2"></span></i> <input type="text" data-kt-ecommerce-category-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Search Keyword">
            </div>
        </div>
    </div>

    <div class="card-body pt-0">

        <div id="kt_ecommerce_category_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_ecommerce_category_table">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                            <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1" aria-label="" style="width: 29.8906px;">
                                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                    <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_category_table .form-check-input" value="1">
                                </div>
                            </th>
                            <th class="min-w-250px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1" aria-label="Choices: activate to sort column ascending" style="width: 825.984px;">
                                Choice Names</th>
                            <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1" aria-label="Order: activate to sort column ascending" style="width: 220.047px;">
                                Order</th>
                            <th class="min-w-150px sorting" tabindex="0" aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 220.047px;">
                                Status</th>
                            <th class="text-end min-w-70px sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 150.828px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody class="fw-semibold text-gray-600">
                        @foreach ($items as $item)
                        <tr>
                            <td>
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1">
                                </div>
                            </td>
                            <td>
                                <div class="d-flex">

                                    <div class="ms-5">
                                        <a href="{{route('choices.edit',$item->id)}}" class="text-gray-800 text-hover-primary fs-5 fw-bold mb-1" data-kt-ecommerce-category-filter="category_name">{{ $item->name }}</a>

                                        <div class="text-muted fs-7 fw-bold">
                                            @foreach ($item->options as $key => $option)
                                            {{ $key !== 0 && $key < 6 ? __(', ') . $option->name : $option->name }}
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="text-muted">{{ $item->order }}</div>
                            </td>
                            <td>
                                {!! statusHtml($item->status) !!}
                            </td>
                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary btn-flex btn-center" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                    Actions
                                    <i class="fas fa-angle-down ms-2 fs-7"></i> </a>
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                    <div class="menu-item px-3 btn btn-sm">
                                        <a href="{{ route('choices.edit', $item->id) }}" class="menu-link px-3">
                                            Edit
                                        </a>
                                    </div>
                                    <div class="menu-item px-3">
                                        <form method="post" action="{{ route('choices.destroy', $item['id']) }}" class="text-center">
                                            @csrf
                                            @method('delete')
                                            <button class="menu-link px-3 btn btn-sm w-100" href="javascript:{}">
                                                Delete
                                            </button>
                                        </form>
                                    </div>

                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
        const choiceAlertBtn = document.getElementById('wc_choice_alert');
        var planName = @php echo json_encode($subscription->plan->name); @endphp;

        choiceAlertBtn.addEventListener('click', e => {
            e.preventDefault();
            Swal.fire({
                html: `<strong class="d-block text-info fs-4 mb-2">Choice Limit Reached!</strong><p class="text-gray-700 fw-semibold">You've reached the maximum number of choices allowed for the <span class="text-info">${planName} plan</span>. Why
        limit your culinary adventure? Upgrade your plan and enjoy infinite choices storage, exclusive cooking tips, and
        early access to exciting new features.</p><a href="/pages/manage-plan" class="btn btn-info my-5">Upgrade Now 🚀</a>`,
                icon: "info",
                buttonsStyling: false,
                confirmButtonText: "Upgrade Now 🚀",
                showConfirmButton: false,
                customClass: {
                    confirmButton: "btn btn-info"
                }
            });
        });
    </script>
@endsection