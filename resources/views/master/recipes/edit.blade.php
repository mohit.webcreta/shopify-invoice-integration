@extends('layout.default')

@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-black fw-bolder my-1 fs-3">Edit Recipe </h1>
                <small>Edit '{{ $recipe->name }}' recipe details</small>
            </div>
            <div class="d-flex gap-4 gap-lg-13">

                <div class="d-flex flex-column">
                    <a href="{{ URL::previous() }}"
                        class="btn btn-outline btn-outline-dashed btn-outline-dark btn-active-light-dark btn-sm">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('wc-content')

    @php
        // get auth data
        $shop = Auth::user();
        // get authenticated shop data
        $items = $shop->api()->rest('GET', '/admin/api/2023-04/products.json');
        $products = $items['body']['container']['products'];
        $productId = $recipe
            ->products()
            ->pluck('product_id')
            ->all();
        
    @endphp

    <form id="kt_ecommerce_add_category_form" class="form d-flex flex-column flex-lg-row"
        action="{{ route('recipes.update', $recipe->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 me-lg-10 mb-7">
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>General</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class=" form-label">Recipe Name</label>
                        <input type="text" name="name" class="form-control mb-2" placeholder="Recipe name"
                            value="{{ optional($recipe)->name }}" id="name">
                        <input type="hidden" name="slug" class="form-control mb-2" placeholder="Recipe slug"
                            value="{{ optional($recipe)->slug }}" id="slug">
                        <input type="hidden" name="user_id" class="form-control mb-2" value="{{ Auth::user()->id }}">

                        @error('choice_name')
                            <div class="fs-7 text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="card card-custom">
                            <label class=" form-label">Description</label>
                            <textarea name="desc" id="kt-ckeditor-1" placeholder="Recipe description">{{ optional($recipe)->desc }}</textarea>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Recipe Choice</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        @php
                            $pivotChoiceIds = $recipe
                                ->options()
                                ->pluck('id')
                                ->toArray();
                        @endphp
                        @if (count($choices) > 0)
                            @foreach ($choices as $choice)
                                <div class="col-sm-12 col-md-6">
                                    <div class="mb-10 fv-row">
                                        <div class="card card-custom">
                                            <label class=" form-label">{{ $choice->name }}</label>
                                            <select class="form-select" data-control="select2"
                                                data-placeholder="Select an option" multiple="multiple"
                                                data-close-on-select="false" name="choices[{{ $choice->id }}][]"
                                                data-allow-clear="true">
                                                <option></option>
                                                @foreach ($choice->options as $option)
                                                    <option value="{{ $option->id }}"
                                                        {{ in_array($option->id, $pivotChoiceIds ?? []) ? 'selected' : '' }}>
                                                        {{ $option->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body text-center border border-dashed border-secondary">
                                        <p class="text-muted fs-6 mb-5">
                                            It appears that you have not yet added any recipe options. To proceed, you will
                                            need to include your preferred recipe choices.
                                        </p>
                                        <a class="btn btn-outline btn-outline-dashed btn-outline-dark btn-active-light-dark btn-sm"
                                            href="{{ route('choices.create') }}">Add Now</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Cooking Information</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <div class="card card-custom">
                            <label class=" form-label">Method</label>
                            <textarea name="cooking_desc" id="kt-ckeditor-2" placeholder="Cooking methods">{{ optional($recipe)->cooking_desc }}</textarea>
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="card card-custom">
                            <label class=" form-label">Add Ingradient</label>
                            <textarea id="kt-ckeditor-3" name="ingredient_desc" placeholder="Ingradient information">{{ optional($recipe)->ingredient_desc }}</textarea>
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="card card-status">
                            <label for="" class="form-label">Serving Number</label>
                            <select class="form-select mb-2 w-25" data-control="select2" data-hide-search="true"
                                data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                name="serving_number">
                                @for ($i = 0; $i < 21; $i++)
                                    <option value="{{ $i }}"
                                        {{ $i == $recipe->serving_number ? 'selected' : '' }}>
                                        {{ $i }}</option>
                                @endfor
                            </select>
                            <div class="text-muted fs-7">Set the serving number.</div>
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="card card-status">
                            <label for="" class="form-label">Preparation Time</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                        name="pt_hh">
                                        @for ($i = 0; $i < 24; $i++)
                                            <option value="{{ $i }}"
                                                {{ $i == $recipe->pt_hh ? 'selected' : '' }}>
                                                {{ $i }}</option>
                                        @endfor
                                    </select>
                                    <div class="text-muted fs-7">Set the hours.</div>

                                </div>
                                <div class="col-sm-4">
                                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                        name="pt_mm">
                                        @for ($i = 0; $i < 60; $i++)
                                            <option value="{{ $i }}"
                                                {{ $i == $recipe->pt_mm ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                    <div class="text-muted fs-7">Set the minutes.</div>

                                </div>
                                <div class="col-sm-4">
                                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                        name="pt_ss">
                                        @for ($i = 00; $i < 60; $i++)
                                            <option value="{{ $i }}"
                                                {{ $i == $recipe->pt_ss ? 'selected' : '' }}>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                    <div class="text-muted fs-7">Set the seconds.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="card card-status">
                            <label for="" class="form-label">Cook Time</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                        name="ct_hh">
                                        @for ($i = 0; $i < 24; $i++)
                                            <option value="{{ $i }}"
                                                {{ $i == $recipe->ct_hh ? 'selected' : '' }}>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                    <div class="text-muted fs-7">Set the hours.</div>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                        name="ct_mm">
                                        @for ($i = 0; $i < 60; $i++)
                                            <option value="{{ $i }}"
                                                {{ $i == $recipe->ct_mm ? 'selected' : '' }}>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                    <div class="text-muted fs-7">Set the minutes.</div>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select"
                                        name="ct_ss">
                                        @for ($i = 00; $i < 60; $i++)
                                            <option value="{{ $i }}"
                                                {{ $i == $recipe->ct_ss ? 'selected' : '' }}>
                                                {{ $i }}
                                            </option>
                                        @endfor
                                    </select>
                                    <div class="text-muted fs-7">Set the seconds.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Image</h2>
                    </div>
                </div>

                <div class="card-body text-center pt-0">
                    @if ($recipe->images !== null && count($recipe->images) > 0)
                        <style>
                            .image-input-placeholder {
                                background-image: url('{{ asset($recipe->images[0]->url) }}');
                            }
                        </style>
                    @else
                        <style>
                            .image-input-placeholder {
                                background-image: url('{{ asset(' images/blank-image.svg') }}');
                            }
                        </style>
                    @endif
                    <div class="image-input image-input-empty image-input-outline image-input-placeholder mb-3"
                        data-kt-image-input="true">
                        <div class="image-input-wrapper w-150px h-150px"></div>

                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                            data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                            <i class="fa-solid fa-pen"></i>

                            <input type="file" name="avatar" accept=".png, .jpg, .jpeg">
                            <input type="hidden" name="avatar_remove">
                        </label>

                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                            data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                            <i class="fa-solid fa-trash"></i>
                        </span>
                    </div>

                    <div class="text-muted fs-7">Set the recipe thumbnail image. Only *.png, *.jpg and *.jpeg image
                        files
                        are accepted</div>
                </div>
            </div>

            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Product Details</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="mb-10 fv-row">
                                <div class="card card-status">
                                    <label for="" class="form-label">Products</label>
                                    <input class="form-control mb-2" value="" id="kt_tagify_users"
                                        name="product_id" />
                                    <div class="text-muted fs-7">Choose recipe related products.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Recipe URL</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <input type="text" name="url" class="form-control mb-2" placeholder="Recipe URL"
                        value="{{ optional($recipe)->url }}">

                    @if (optional($recipe)->url)
                        <a href="{{ optional($recipe)->url }}">Click here to visit the link.</a>
                    @else
                        <div class="text-muted fs-7">Set the Recipe url from other source.</div>
                    @endif
                </div>
            </div>
            <div class="card card-flush py-4">
                <div class="card-header">
                    <div class="card-title">
                        <h2>Video URL</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <input type="text" name="video_url" class="form-control mb-2" placeholder="Video URL"
                        value="{{ optional($recipe)->video_url }}">
                    @if (optional($recipe)->video_url)
                        <a href="{{ optional($recipe)->video_url }}">Click here to visit the link.</a>
                    @else
                        <div class="text-muted fs-7">Set the video url from other source.</div>
                    @endif
                </div>
            </div>

            <div class="d-flex justify-content-between">
                <div class="me-3">

                    <select class="form-select mb-2" data-control="select2" data-hide-search="true"
                        data-placeholder="Select an option" id="kt_ecommerce_add_category_status_select" name="status">
                        @foreach (\App\Enums\BaseStatusEnum::cases() as $status)
                            <option value="{{ $status->value }}" {{ $recipe->status == $status ? 'selected' : '' }}>
                                {{ $status->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="ms-1">

                    <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                        <span class="indicator-label">
                            Save
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>

    <script>
        // Class definition

        var KTCkeditor = function() {
            // Private functions
            var demos = function() {
                ClassicEditor
                    .create(document.querySelector('#kt-ckeditor-1'))
                    .then(editor => {})
                    .catch(error => {});
            }

            var demos2 = function() {
                ClassicEditor
                    .create(document.querySelector('#kt-ckeditor-2'))
                    .then(editor => {})
                    .catch(error => {});
            }

            var demos3 = function() {
                ClassicEditor
                    .create(document.querySelector('#kt-ckeditor-3'))
                    .then(editor => {})
                    .catch(error => {});
            }

            return {
                // public functions
                init: function() {
                    demos();
                    demos2();
                    demos3();
                }
            };
        }();

        var KTBootstrapMultipleSelectsplitter = function() {

            // Private functions
            var demos = function() {
                // minimum setup
                $('#kt_multipleselectsplitter_1, #kt_multipleselectsplitter_2').multiselectsplitter();
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        // Initialization
        jQuery(document).ready(function() {
            KTCkeditor.init();
            KTBootstrapMultipleSelectsplitter.init();

            $('#name').change(function() {
                var Text = $('#name').val();
                Text = Text.toLowerCase();
                Text = Text.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                    .trim()
                    .replace(/\s+/g, '-') // collapse whitespace and replace by -
                    .replace(/-+/g, '-'); // collapse dashes
                $("#slug").val(Text);
            });
        });

        // image avatar 
        var avatar1 = new KTImageInput('kt_image_1');
    </script>

    <script>
        var inputElm = document.querySelector('#kt_tagify_users');

        // get products and pass from php to js 
        var products = <?php echo json_encode($products); ?>;
        var productsId = <?php echo json_encode($productId, true); ?>

        var usersList = [];
        var selectedusersList = [];
        // create an object for products

        products.forEach(function(pr) {
            var value = pr.id;
            var handle = pr.handle;
            var name = pr.title;
            var avatar = pr.image ? pr.image.src : 'https://cdn3d.iconscout.com/3d/premium/thumb/product-5806313-4863042.png';
            var prObj = {
                value: value.toString(),
                handle: handle,
                name: name,
                avatar: avatar,
            };
            usersList.push(prObj);


            // check data is selected or not
            if (productsId.includes(value)) {
                selectedusersList.push(prObj);
            }
        });

        function tagTemplate(tagData) {
            return `
                <tag title="${(tagData.title || tagData.handle)}"
                        contenteditable='false'
                        spellcheck='false'
                        tabIndex="-1"
                        class="${this.settings.classNames.tag} ${tagData.class ? tagData.class : ""}"
                        ${this.getAttributes(tagData)}>
                    <x title='' class='tagify__tag__removeBtn' role='button' aria-label='remove tag'></x>
                    <div class="d-flex align-items-center">
                        <div class='tagify__tag__avatar-wrap ps-0'>
                            <img onerror="this.style.visibility='hidden'" class="rounded-circle w-25px me-2" src="${tagData.avatar}">
                        </div>
                        <span class='tagify__tag-text'>${tagData.name}</span>
                    </div>
                </tag>
            `
        }

        function suggestionItemTemplate(tagData) {
            return `
                <div ${this.getAttributes(tagData)}
                    class='tagify__dropdown__item d-flex align-items-center ${tagData.class ? tagData.class : ""}'
                    tabindex="0"
                    role="option">

                    ${tagData.avatar ? `
                                                    <div class='tagify__dropdown__item__avatar-wrap me-2'>
                                                        <img onerror="this.style.visibility='hidden'"  class="rounded-circle w-50px me-2" src="${tagData.avatar}">
                                                    </div>` : ''
                        }

                    <div class="d-flex flex-column">
                        <strong>${tagData.name}</strong>
                        <span>${tagData.handle}</span>
                    </div>
                </div>
            `
        }

        // initialize Tagify on the above input node reference
        var tagify = new Tagify(inputElm, {
            tagTextProp: 'name', // very important since a custom template is used with this property as text. allows typing a "value" or a "name" to match input with whitelist
            enforceWhitelist: true,
            skipInvalid: true, // do not remporarily add invalid tags
            dropdown: {
                closeOnSelect: false,
                enabled: 0,
                classname: 'users-list',
                searchKeys: ['name',
                    'handle'
                ] // very important to set by which keys to search for suggesttions when typing
            },
            templates: {
                tag: tagTemplate,
                dropdownItem: suggestionItemTemplate
            },
            whitelist: usersList
        })

        tagify.on('dropdown:show dropdown:updated', onDropdownShow)
        tagify.on('dropdown:select', onSelectSuggestion)

        var addAllSuggestionsElm;

        function onDropdownShow(e) {
            var dropdownContentElm = e.detail.tagify.DOM.dropdown.content;

            if (tagify.suggestedListItems.length > 1) {
                addAllSuggestionsElm = getAddAllSuggestionsElm();

                // insert "addAllSuggestionsElm" as the first element in the suggestions list
                dropdownContentElm.insertBefore(addAllSuggestionsElm, dropdownContentElm.firstChild)
            }
        }

        function onSelectSuggestion(e) {
            if (e.detail.elm == addAllSuggestionsElm)
                tagify.dropdown.selectAll.call(tagify);
        }

        // create a "add all" custom suggestion element every time the dropdown changes
        function getAddAllSuggestionsElm() {
            // suggestions items should be based on "dropdownItem" template
            return tagify.parseTemplate('dropdownItem', [{
                class: "addAll",
                name: "Add all",
                handle: tagify.settings.whitelist.reduce(function(remainingSuggestions, item) {
                    return tagify.isTagDuplicate(item.value) ? remainingSuggestions :
                        remainingSuggestions + 1
                }, 0) + " Products"
            }])
        }

        tagify.addTags(selectedusersList);
    </script>
@endsection
