@extends('layout.default')

@php
    // get auth data
    $shop = Auth::user();
    $subscription = App\Models\Subscription::where('user_id', '=', $shop->id)
        ->orderBy('id', 'desc')
        ->first();
    // get authenticated shop data
    $planConfig = $subscription->plan->plan_config;
    $totalRecipes = count($shop->recipes);
    $exceedRecipe = $totalRecipes == (int) $planConfig->number_recipe_allow ? true : false;
@endphp

@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Recipes</h1>
                <small>{{ $items->count() }} results found</small>
            </div>
            <div class="d-flex gap-4 gap-lg-13">
                <div class="d-flex flex-column">
                    @if ($exceedRecipe)
                        <button type="button" class="btn btn-secondary btn-sm" id="wc_recipe_alert">
                            {{ __('Add New Recipe') }}
                        </button>
                    @else
                        <a href="{{ route('recipes.create') }}" class="btn btn-secondary btn-sm">
                            {{ __('Add New Recipe') }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('wc-content')
    <div class="card card-flush">
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4"><span class="path1"></span><span
                            class="path2"></span></i> <input type="text" data-kt-ecommerce-category-filter="search"
                        class="form-control form-control-solid w-250px ps-12" placeholder="Search Keyword">
                </div>
            </div>
        </div>

        <div class="card-body pt-0">

            <div id="kt_ecommerce_category_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                        id="kt_ecommerce_category_table">
                        <thead>
                            <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                <th class="w-10px pe-2 text-start sorting_disabled" rowspan="1" colspan="1"
                                    aria-label="">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                        <input class="form-check-input" type="checkbox" data-kt-check="true"
                                            data-kt-check-target="#kt_ecommerce_category_table .form-check-input"
                                            value="1">
                                    </div>
                                </th>
                                <th class="min-w-250px text-start sorting" tabindex="0"
                                    aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                    aria-label="Choices: activate to sort column ascending">
                                    Recipe Names</th>
                                <th class="min-w-150px text-start sorting" tabindex="0"
                                    aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                    aria-label="Order: activate to sort column ascending">
                                    URL</th>
                                <th class="min-w-150px text-start sorting" tabindex="0"
                                    aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                    aria-label="Status: activate to sort column ascending">
                                    Status</th>
                                <th class="min-w-70px text-end sorting_disabled" rowspan="1" colspan="1"
                                    aria-label="Actions">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="fw-semibold text-gray-600">
                            @foreach ($items as $item)
                                <tr>
                                    <td>
                                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="1">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <a href="{{ route('recipes.edit', $item->id) }}" class="symbol symbol-50px">

                                                @if (count($item->images) > 0)
                                                    <span class="symbol-label"
                                                        style="background-image:url('{{ asset($item->images[0]->url) }}');"></span>
                                                @else
                                                    <span class="symbol-label"
                                                        style="background-image:url({{ asset('images/blank-image.svg') }});"></span>
                                                @endif
                                            </a>

                                            <div class="ms-5">
                                                <a href="{{ route('recipes.edit', $item->id) }}"
                                                    class="text-gray-800 text-hover-primary fs-5 fw-bold"
                                                    data-kt-ecommerce-product-filter="product_name">{{ $item->name }}</a>
                                                <p class="fs-8">
                                                    @foreach ($item->options as $key => $option)
                                                        {{ $key < 6 ? ($key !== 0 ? __(', ') . $option->name : $option->name) : '' }}
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if ($item->url)
                                            <a class="text-primary" href="{{ url($item->url) }}">
                                                <i class="fas fa-link text-primary"></i></a>
                                        @else
                                            <a class="text-muted">
                                                <i class="fas fa-link text-muted"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        {!! statusHtml($item->status) !!}
                                    </td>
                                    <td class="text-end">
                                        <a href="#"
                                            class="btn btn-sm btn-light btn-active-light-primary btn-flex btn-center"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            Actions
                                            <i class="fas fa-angle-down ms-2 fs-7"></i> </a>
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                            data-kt-menu="true">
                                            <div class="menu-item px-3 btn btn-sm">
                                                <a href="{{ route('recipes.edit', $item->id) }}" class="menu-link px-3">
                                                    Edit
                                                </a>
                                            </div>
                                            <div class="menu-item px-3">
                                                <form method="post" action="{{ route('recipes.destroy', $item['id']) }}"
                                                    class="text-center">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="menu-link px-3 btn btn-sm w-100" href="javascript:{}">
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
<script>
        const recipeAlertBtn = document.getElementById('wc_recipe_alert');
        const choiceAlertBtn = document.getElementById('wc_choice_alert');
        var totalRecipe = @php echo json_encode($totalRecipes); @endphp;
        var planName = @php echo json_encode($subscription->plan->name); @endphp;

        recipeAlertBtn.addEventListener('click', e => {
            e.preventDefault();
            Swal.fire({
                html: `<strong class="d-block text-info fs-4 mb-2">Recipe Limit Reached!</strong><p class="text-gray-700 fw-semibold">You've reached the maximum number of recipes allowed for the <span class="text-info">${planName} plan</span>. Why
        limit your culinary adventure? Upgrade your plan and enjoy infinite recipe storage, exclusive cooking tips, and
        early access to exciting new features.</p><a href="/pages/manage-plan" class="btn btn-info my-5">Upgrade Now 🚀</a>`,
                icon: "info",
                buttonsStyling: false,
                confirmButtonText: "Upgrade Now 🚀",
                showConfirmButton: false,
                customClass: {
                    confirmButton: "btn btn-info"
                }
            });
        });

        choiceAlertBtn.addEventListener('click', e => {
            e.preventDefault();
            Swal.fire({
                html: `<strong class="d-block text-info fs-4 mb-2">Choice Limit Reached!</strong><p class="text-gray-700 fw-semibold">You've reached the maximum number of choices allowed for the <span class="text-info">${planName} plan</span>. Why
        limit your culinary adventure? Upgrade your plan and enjoy infinite choices storage, exclusive cooking tips, and
        early access to exciting new features.</p><a href="/pages/manage-plan" class="btn btn-info my-5">Upgrade Now 🚀</a>`,
                icon: "info",
                buttonsStyling: false,
                confirmButtonText: "Upgrade Now 🚀",
                showConfirmButton: false,
                customClass: {
                    confirmButton: "btn btn-info"
                }
            });
        });
    </script>
@endsection