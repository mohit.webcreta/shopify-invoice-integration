<div class="alert alert-dismissible bg-dark border border-danger border-dashed d-flex flex-column flex-sm-row p-5 mb-10"
    style="position: fixed; z-index: 100001; bottom: 50px; right: 20px" id="plan-alert">
    <span class="svg-icon svg-icon-2hx svg-icon-danger my-auto me-3">
        <i class="bi bi-exclamation-circle-fill text-danger fs-2"></i>
    </span>
    <div class="d-flex flex-column pe-0 pe-sm-10">
        <h5 class="align-center my-auto text-danger" id="plan-alert-msg"></h5>
    </div>
    <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto"
        data-bs-dismiss="alert">
        <i class="bi bi-x fs-1 text-danger"></i>
    </button>
</div>
