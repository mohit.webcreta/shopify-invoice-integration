@extends('shopify-app::layouts.default')

@extends('layout.default')

@section('wc-head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.2.3/axios.min.js"></script>
@endsection

@section('toolbar')
<div class="toolbar py-5 py-lg-15" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-3">
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Contact Support</h1>
        </div>
    </div>
</div>
@endsection


@section('wc-content')
    @php
        $shop = Auth::user();
        $subscription = App\Models\Subscription::where('user_id', '=', $shop->id)->first();
    @endphp

    @if ($subscription == null)
        <script type="text/javascript">
            window.location = "{{ route('plan') }}";
        </script>
    @else
        <div class="row g-5 g-xl-10 mb-5 mb-xl-0">
            <div class="col-lg-12">
                <div class="card mb-12">
                    <div class="card-body flex-column p-5">
                        <div class="d-flex align-items-center p-5 p-lg-15">
                            <div class="">
                                <h5 class="fs-6 fs-lg-5 text-gray-800 fw-normal mb-5 mb-lg-10">

                                    <p class="lh-base">Welcome to the <b>{{ $config->title }}</b> Contact Support page!</p>
                                    <p class="lh-base">
                                        We value your feedback, questions, and suggestions as they help us improve and
                                        enhance your experience with our third-party app dedicated to bringing you the best
                                        in culinary delights. Whether you're a seasoned chef, a home cook, or someone who
                                        simply enjoys experimenting with new recipes, we're here to assist you in every way
                                        possible.
                                    </p>
                                    <p class="lh-base">
                                        If you have any queries regarding our app's features, functionality, or usage, our
                                        dedicated support team is just a message away. We are committed to providing timely
                                        and helpful responses to ensure you get the most out of "{{ $config->title }}"
                                    </p>
                                    <p class="lh-base">
                                        Have a recipe idea you'd like to share or a specific cuisine you'd love to see
                                        featured? Feel free to reach out to us! Your culinary passions and preferences are
                                        of utmost importance to us, and we're always eager to explore new possibilities.
                                    </p>
                                    <p class="lh-base">
                                        Should you encounter any technical issues or require assistance with
                                        troubleshooting, please don't hesitate to contact us. Our skilled technical support
                                        team is ready to lend a hand and resolve any challenges you may face while using our
                                        app.
                                    </p>
                                    <p class="lh-base">
                                        Your privacy is crucial to us, and we want you to feel secure while using
                                        "{{ $config->title }}" If you have any concerns or questions regarding our privacy
                                        practices, please
                                        reach out, and we will gladly address them.
                                    </p>
                                    <p class="lh-base">
                                        Collaborations and partnerships are always welcome at "{{ $config->title }}" If you
                                        are a
                                        fellow food enthusiast, a blogger, a chef, or a food-related business, and wish to
                                        explore potential collaborations, do get in touch. We believe in building strong
                                        relationships within the culinary community and would be thrilled to work together
                                        to create delightful experiences for our users.
                                    </p>
                                    <p class="lh-base">
                                        Thank you for being a part of our "{{ $config->title }}" community! Your continued
                                        support and
                                        feedback drive us to constantly evolve and make our app even better. Together, let's
                                        embark on a journey filled with delectable recipes and unforgettable culinary
                                        adventures!
                                    </p>
                                </h5>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="position-relative w-100 mt-5">
                                            <i
                                                class="fa-regular fa-envelope text-primary position-absolute top-50 translate-middle ms-8 fs-6"></i>
                                            <input type="text"
                                                class="form-control fs-4 py-4 ps-14 text-gray-700 placeholder-gray-400 mw-500px"
                                                name="search" value="{{ $config->email }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="position-relative w-100 mt-5">
                                            <i
                                                class="fa-solid fa-phone text-primary position-absolute top-50 translate-middle ms-8 fs-6"></i>
                                            <input type="text"
                                                class="form-control fs-4 py-4 ps-14 text-gray-700 placeholder-gray-400 mw-500px"
                                                name="search" value="{{ $config->contact }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
