@extends('shopify-app::layouts.default')

@extends('layout.default')

@section('wc-head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.2.3/axios.min.js"></script>
@endsection

@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
    </div>
@endsection


@section('wc-content')
    @php
        $shop = Auth::user();
        $subscription = App\Models\Subscription::where('user_id', '=', $shop->id)
            ->orderBy('id', 'desc')
            ->first();
    @endphp

    @if ($subscription == null)
        <script type="text/javascript">
            window.location = "{{ route('plan') }}";
        </script>
    @else
        @php
            $storeLogs = App\Models\StoreLog::where('user_id', '=', $shop->id)
                ->orderBy('id', 'desc')
                ->take(5)
                ->get();

            $totalLogs = count($storeLogs);
            $planConfig = $subscription->plan->plan_config;
            $totalRecipes = count($shop->recipes);
            $totalChoices = count($shop->choices);
            $exceedRecipe = $totalRecipes == (int) $planConfig->number_recipe_allow ? true : false;
            $exceedChoice = $totalChoices == (int) $planConfig->number_choice_allow ? true : false;
            $pageExist = App\Models\StoreLog::where('user_id', '=', $shop->id)
                ->where('name', '=', 'Recipe Link')
                ->first();

        @endphp
        <div class="row g-5 g-xl-10 mb-5 mb-xl-0">
            <div class="col-xl-8 col-lg-8">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card h-125px card-xl-stretch bg-white">
                            <div class="card-body d-flex align-items-center justify-content-between">
                                <div class="me-2">
                                    <h2 class="fs-2 text-black fw-bolder">Liquid Page</h2>
                                    <div class="text-gray-600 fs-6 mb-3">By one step, add your recipe listing page to
                                        Shopify.</div>
                                </div>
                                @if ($pageExist)
                                    <button type="button" class="btn btn-secondary btn-sm fw-semibold"
                                        id="wc_liquid_alert">
                                        Add
                                    </button>
                                @else
                                    @if ($totalRecipes > 0)
                                        <a href="{{ route('upload.page') }}"
                                            class="btn btn-secondary btn-sm fw-semibold">Add</a>
                                    @else
                                        <button type="button" class="btn btn-secondary btn-sm fw-semibold"
                                            id="wc_empty_liquid_alert">
                                            Add
                                        </button>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card h-125px card-xl-stretch bg-white">
                            <div class="card-body d-flex align-items-center justify-content-between">
                                <div class="me-2">
                                    <h2 class="fs-2 text-black fw-bolder">Add Recipe</h2>
                                    <div class="text-gray-600 fs-6 mb-3">Increase sales with the shoppable recipe cards
                                        feature in WC
                                        Recipe!</div>
                                </div>
                                @if ($exceedRecipe)
                                    <button type="button" class="btn btn-secondary btn-sm fw-semibold"
                                        id="wc_recipe_alert">
                                        Add
                                    </button>
                                @else
                                    <a href="{{ route('recipes.create') }}"
                                        class="btn btn-secondary btn-sm fw-semibold">Add</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row gx-5 gx-xl-8 mt-5">
                    <div class="col-sm-12 col-md-6">
                        <a href="#" class="card card-xxl-stretch bg-secondary">
                            <div class="card-body d-flex flex-column justify-content-between">
                                <span class="svg-icon svg-icon-dark svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path
                                                d="M6,9 L6,15 C6,16.6568542 7.34314575,18 9,18 L15,18 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L6,9 Z M17,16 L17,10 C17,8.34314575 15.6568542,7 14,7 L8,7 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L17,16 Z"
                                                fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path
                                                d="M9.27272727,9 L13.7272727,9 C14.5522847,9 15,9.44771525 15,10.2727273 L15,14.7272727 C15,15.5522847 14.5522847,16 13.7272727,16 L9.27272727,16 C8.44771525,16 8,15.5522847 8,14.7272727 L8,10.2727273 C8,9.44771525 8.44771525,9 9.27272727,9 Z"
                                                fill="#000000" />
                                        </g>
                                    </svg>
                                </span>
                                <div class="d-flex flex-column">
                                    <div class="text-dark fw-bold fs-1 mb-0 mt-5">
                                        {{ $count['recipe'] }}
                                    </div>
                                    <div class="text-gray-700 fw-semibold fs-6">
                                        Total Recipes </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <a href="#" class="card card-xxl-stretch bg-secondary">
                            <div class="card-body d-flex flex-column justify-content-between">
                                <span class="svg-icon svg-icon-dark svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M9,10 L9,19 L5,19 L5,10 L5,6 L18,6 L18,10 L9,10 Z" fill="#000000"
                                                transform="translate(11.500000, 12.500000) scale(-1, 1) translate(-11.500000, -12.500000) " />
                                            <circle fill="#000000" opacity="0.3" cx="8" cy="16"
                                                r="2" />
                                        </g>
                                    </svg>
                                </span>
                                <div class="d-flex flex-column">
                                    <div class="text-dark fw-bold fs-1 mb-0 mt-5">
                                        {{ $count['choice'] }}
                                    </div>
                                    <div class="text-gray-700 fw-semibold fs-6">
                                        Total Choices </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="card mt-5 mb-5 mb-xl-10">
                    <div class="card-header card-header-stretch border-bottom border-gray-200">
                        <div class="card-title">
                            <h3 class="fw-bold m-0">Log History</h3>
                        </div>

                        <div class="card-toolbar m-0">
                            <ul class="nav nav-stretch nav-line-tabs border-transparent" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a id="kt_billing_6months_tab" class="nav-link fs-5 fw-semibold me-3 active"
                                        data-bs-toggle="tab" role="tab" href="#kt_billing_months"
                                        aria-selected="true">
                                        Month
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-content">
                        <div id="kt_billing_months" class="card-body p-0 tab-pane fade show active" role="tabpanel"
                            aria-labelledby="kt_billing_months">
                            <div class="table-responsive">
                                <table class="table table-row-bordered align-middle gy-4 gs-9">
                                    <thead
                                        class="border-bottom border-gray-200 fs-6 text-gray-600 fw-bold bg-light bg-opacity-75">
                                        <tr>
                                            <td class="min-w-150px">No.</td>
                                            <td class="min-w-150px">Module Name</td>
                                            <td class="min-w-250px">Description</td>
                                            <td class="min-w-150px">Date</td>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">
                                        @foreach ($storeLogs as $key => $item)
                                            <tr>
                                                <td class="text-gray-700 fw-bold">{{ $totalLogs-- }}</td>
                                                <td class="text-gray-700 fw-bold"><a
                                                        href="#">{{ $item->name }}</a></td>
                                                <td class="text-gray-700 fw-bold">{!! $item->value !!}</td>
                                                <td class="text-gray-700 fw-bold">
                                                    {{ $item->created_at->format('d, F, Y \a\t H:i') }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4">
                <div class="card h-125px card-xl-stretch bg-white">
                    <div class="card-body d-flex align-items-center justify-content-between">
                        <div class="me-2">
                            <h2 class="fs-2 text-black fw-bolder">Add Choice</h2>
                            <div class="text-gray-600 fs-6 mb-3">Add recipe choices to attract more details!</div>
                        </div>
                        @if ($exceedChoice)
                            <button type="button" class="btn btn-secondary btn-sm fw-semibold" id="wc_choice_alert">
                                Add
                            </button>
                        @else
                            <a href="{{ route('choices.create') }}" class="btn btn-secondary btn-sm fw-semibold">Add</a>
                        @endif
                    </div>
                </div>

                <div class="card bg-white mt-5 py-3 bg-secondary">
                    <div class="card-body d-flex align-items-center justify-content-between">
                        <div class="me-2">
                            <h2 class="fs-2 text-black fw-bolder">Thanks for using {{ $config->title }}!</h2>
                            <div class="text-gray-800 fs-6 mb-5">Hi! We are always here to help you for any kind of helps.
                                You
                                can reach us or contact us</div>
                            <a class="btn btn-info btn-sm" href="{{ route('pages.support') }}">Contact Supoort</a>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <div class="toast show py-2 px-1 mt-10" role="alert" aria-live="assertive" aria-atomic="true">
                        <div class="toast-header">
                            <strong class="me-auto text-dark">Plan Details</strong>
                            <small>{{ dateDiff($subscription->end_date) }} days left</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast"
                                aria-label="Close"></button>
                        </div>
                        <div class="toast-body text-dark">
                            You have selected the <b class="text-info">{{ $subscription->plan->name }} plan</b>, which
                            will expire on
                            <b class="text-info">{{ date('M d,Y', strtotime($subscription->end_date)) }}</b>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection


@section('scripts')
    {{-- // check recipe limit is exceed or not --}}
    @if ($exceedRecipe)
        <script>
            const recipeAlertBtn = document.getElementById('wc_recipe_alert');
            var planName = @php echo json_encode($subscription->plan->name); @endphp;

            recipeAlertBtn.addEventListener('click', e => {
                e.preventDefault();
                Swal.fire({
                    html: `<strong class="d-block text-info fs-4 mb-2">Recipe Limit Reached!</strong><p class="text-gray-700 fw-semibold">You've reached the maximum number of recipes allowed for the <span class="text-info">${planName} plan</span>. Why
        limit your culinary adventure? Upgrade your plan and enjoy infinite recipe storage, exclusive cooking tips, and
        early access to exciting new features.</p><a href="/pages/manage-plan" class="btn btn-info my-5">Upgrade Now 🚀</a>`,
                    icon: 'info',
                    buttonsStyling: false,
                    confirmButtonText: "Upgrade Now 🚀",
                    showConfirmButton: false,
                    customClass: {
                        confirmButton: "btn btn-info"
                    }
                });
            });
        </script>
    @endif

    {{-- // check recipe choice limit is exceed or not --}}
    @if ($exceedChoice)
        <script>
            const choiceAlertBtn = document.getElementById('wc_choice_alert');
            var totalRecipe = @php echo json_encode($totalRecipes); @endphp;
            var planName = @php echo json_encode($subscription->plan->name); @endphp;
            choiceAlertBtn.addEventListener('click', e => {
                e.preventDefault();
                Swal.fire({
                    html: `<strong class="d-block text-info fs-4 mb-2">Choice Limit Reached!</strong><p class="text-gray-700 fw-semibold">You've reached the maximum number of choices allowed for the <span class="text-info">${planName} plan</span>. Why
        limit your culinary adventure? Upgrade your plan and enjoy infinite choices storage, exclusive cooking tips, and
        early access to exciting new features.</p><a href="/pages/manage-plan" class="btn btn-info my-5">Upgrade Now 🚀</a>`,
                    icon: 'info',
                    buttonsStyling: false,
                    confirmButtonText: "Upgrade Now 🚀",
                    showConfirmButton: false,
                    customClass: {
                        confirmButton: "btn btn-info"
                    }
                });
            });
        </script>
    @endif

    {{-- // check recipe is empty or not to create a page --}}
    @if ($totalRecipes == 0)
        <script>
            const emptyLiquidPage = document.getElementById('wc_empty_liquid_alert');
            var planName = @php echo json_encode($subscription->plan->name); @endphp;
            emptyLiquidPage.addEventListener('click', e => {
                e.preventDefault();
                Swal.fire({
                    html: `<strong class="d-block text-info fs-4 mb-2">Recipes Empty!</strong><p class="text-gray-700 fw-semibold">You had not yet added any <span class="text-info">recipes</span>. To create a <span class="text-primary">view page</span>, please first add a recipe.</p><a class="btn btn-info mt-3" href="/recipes/create">Add Now <i class="fa-solid fa-arrow-right-long ms-1"></i></a>`,
                    icon: 'info',
                    buttonsStyling: false,
                    confirmButtonText: "Upgrade Now 🚀",
                    showConfirmButton: false,
                    customClass: {
                        confirmButton: "btn btn-info"
                    }
                });
            });
        </script>
    @endif
    {{-- // check page is exist or not --}}
    @if ($pageExist)
        <script>
            const liquidAlertBtn = document.getElementById('wc_liquid_alert');
            var planName = @php echo json_encode($subscription->plan->name); @endphp;
            var pageValue = @php echo json_encode($pageExist->value) @endphp;

            liquidAlertBtn.addEventListener('click', e => {
                e.preventDefault();
                Swal.fire({
                    html: `${pageValue}`,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Upgrade Now 🚀",
                    showConfirmButton: false,
                    customClass: {
                        confirmButton: "btn btn-info"
                    }
                });
            });
        </script>
    @endif
@endsection
