@extends('shopify-app::layouts.default')

@extends('layout.default')

@section('toolbar')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
    </div>
@endsection


@section('wc-content')
    @php
        $shop = Auth::user();
        $users = $shop->api()->rest('GET', '/admin/api/2021-07/shop.json'); // store's data
$subscription = App\Models\Subscription::where('user_id', '=', $shop->id)
    ->orderBy('id', 'desc')
            ->first();
    @endphp


    <div class="row g-6 mb-6 g-xl-9 mb-xl-9">

        <div class="col-md-6">
            <div class="card ">
                <div class="card-body d-flex flex-center flex-column py-9 px-5">
                    <img src="{{ asset('images/layouts/layout-1.png') }}" alt="image" width="100%">
                    <div class="d-block mt-5">
                    <button class="btn btn-sm btn-light-primary btn-flex btn-center" data-kt-follow-btn="true">
                        <i class="ki-outline ki-check following fs-3"></i>
                        <span class="indicator-label">
                            Following</span>
                    </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card ">
                <div class="card-body d-flex flex-center flex-column py-9 px-5">
                    <img src="{{ asset('images/layouts/layout-2.png') }}" alt="image" width="100%">
                    <div class="d-block mt-5">
                    <button class="btn btn-sm btn-light-primary btn-flex btn-center" data-kt-follow-btn="true">
                        <i class="ki-outline ki-check following fs-3"></i>
                        <span class="indicator-label">
                            Following</span>
                    </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card ">
                <div class="card-body d-flex flex-center flex-column py-9 px-5">
                    <img src="{{ asset('images/layouts/layout-3.png') }}" alt="image" width="100%">
                    <div class="d-block mt-5">
                    <button class="btn btn-sm btn-light-primary btn-flex btn-center" data-kt-follow-btn="true">
                        <i class="ki-outline ki-check following fs-3"></i>
                        <span class="indicator-label">
                            Following</span>
                    </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
