@extends('shopify-app::layouts.default')

@extends('layout.default')

@section('toolbar')
<div class="toolbar py-5 py-lg-15" id="kt_toolbar">
</div>
@endsection


@section('wc-content')
    @php
        $shop = Auth::user();
        $users = $shop->api()->rest('GET', '/admin/api/2021-07/shop.json'); // store's data
$subscription = App\Models\Subscription::where('user_id', '=', $shop->id)
    ->orderBy('id', 'desc')
    ->first();
$subscriptions = App\Models\Subscription::where('user_id', '=', $shop->id)
    ->orderBy('id', 'desc')
    ->get();

$plan = $subscription->plan; // store plan
$totalRecipe = App\Models\Recipe::where('user_id', $shop->id)->count(); //store recipe
$totalChoice = App\Models\Choice::where('user_id', $shop->id)->count(); //store choice

$user = $users['body']->container['shop'];
        // dd($user);
    @endphp


    <div class="card mb-5 mb-xl-10">
        <div class="card-body pt-9 pb-0">
            <div class="d-flex flex-wrap flex-sm-nowrap">
                <div class="me-7 mb-4">
                    <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative border">
                        <img src="https://i.pinimg.com/originals/07/10/4f/07104f00872551c28907f821554ec4ad.png"
                            alt="image">
                        <div
                            class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-body h-20px w-20px">
                        </div>
                    </div>
                </div>

                <div class="flex-grow-1">
                    <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                        <div class="d-flex flex-column">
                            <div class="d-flex align-items-center mb-2">
                                <a href="#"
                                    class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">{{ $user['name'] }}</a>
                                <a href="#"><i class="ki-outline ki-verify fs-1 text-primary"></i></a>
                            </div>

                            <div class="d-flex flex-wrap fw-semibold fs-6 mb-4 pe-2">
                                <a href="#"
                                    class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                    <i class="fa-solid fa-circle-user fs-6 me-2"></i> {{ $user['shop_owner'] }}
                                </a>
                                @if ($user['city'])
                                    <a href="#"
                                        class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                        <i class="fa-solid fa-city fs-6 me-2"></i> {{ $user['city'] }}
                                    </a>
                                @endif
                                @if ($user['email'])
                                    <a href="#"
                                        class="d-flex align-items-center text-gray-400 text-hover-primary mb-2">
                                        <i class="fa-solid fa-envelope fs-6 me-2"></i>{{ $user['email'] }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap flex-stack">
                        <div class="d-flex flex-column flex-grow-1 pe-8">
                            <div class="d-flex flex-wrap">
                                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                    <div class="d-flex align-items-center">
                                        <i class="ki-outline ki-arrow-down fs-3 text-danger me-2"></i>
                                        <div class="fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="80"
                                            data-kt-initialized="1">{{ $totalRecipe }}</div>
                                    </div>

                                    <div class="fw-semibold fs-6 text-gray-400">Recipes</div>
                                </div>

                                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                    <div class="d-flex align-items-center">
                                        <i class="ki-outline ki-arrow-up fs-3 text-success me-2"></i>
                                        <div class="fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="60"
                                            data-kt-countup-prefix="%" data-kt-initialized="1">{{ $totalChoice }}</div>
                                    </div>

                                    <div class="fw-semibold fs-6 text-gray-400">Choice</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-5 mb-xl-10">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="mb-2">Active until {{ date('M d, Y', strtotime($subscription->end_date)) }}</h3>
                    <p class="fs-6 text-gray-700 fw-semibold mb-6 mb-lg-15">We will send you a notification upon
                        Subscription expiration </p>
                </div>
                <div class="col-lg-7">
                    <div class="fs-5 mb-2">
                        <span class="text-gray-800 fw-bold me-1">${{ $plan->price }}</span>
                        <span class="text-gray-700 fw-semibold">Per Month</span>
                    </div>

                    <div class="fs-6 text-gray-700 fw-semibold">
                        You have selected the {{ $plan->name }} plan.
                    </div>
                </div>

                <div class="col-lg-5">
                    {{-- <div class="d-flex text-muted fw-bold fs-5 mb-3">
                        <span class="flex-grow-1 text-gray-800">Recipes</span>
                        <span class="text-gray-800">86 of 100 Used</span>
                    </div>

                    <div class="progress h-8px bg-light-primary mb-2">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 86%" aria-valuenow="86"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                    <div class="fs-6 text-gray-600 fw-semibold mb-10">14 Users remaining until your plan requires update
                    </div> --}}

                    <div class="d-flex justify-content-end pb-0 px-0">
                        <a class="btn btn-sm btn-primary" href="{{ route('pages.manageplan') }}">Upgrade Plan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-5 mb-xl-10">
        <div class="card-header card-header-stretch border-bottom border-gray-200">
            <div class="card-title">
                <h3 class="fw-bold m-0">Plan History</h3>
            </div>

            <div class="card-toolbar m-0">
                <ul class="nav nav-stretch nav-line-tabs border-transparent" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a id="kt_billing_6months_tab" class="nav-link fs-5 fw-semibold me-3 active" data-bs-toggle="tab"
                            role="tab" href="#kt_billing_months" aria-selected="true">
                            Month
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="tab-content">
            <div id="kt_billing_months" class="card-body p-0 tab-pane fade show active" role="tabpanel"
                aria-labelledby="kt_billing_months">
                <div class="table-responsive">
                    <table class="table table-row-bordered align-middle gy-4 gs-9">
                        <thead class="border-bottom border-gray-200 fs-6 text-gray-600 fw-bold bg-light bg-opacity-75">
                            <tr>
                                <td class="min-w-150px">Start Date</td>
                                <td class="min-w-150px">End Date</td>
                                <td class="min-w-250px">Name</td>
                                <td class="min-w-150px">Amount</td>
                                <td class="min-w-150px">Plan Type</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody class="fw-semibold text-gray-600">
                            @foreach ($subscriptions as $item)
                                <tr>
                                    <td>{{ date('M d, Y', strtotime($item->start_date)) }}</td>
                                    <td>{{ date('M d, Y', strtotime($item->end_date)) }}</td>
                                    <td><a href="#">{{ $item->plan->name }}</a></td>
                                    <td>${{ $item->plan->price }}</td>
                                    <td>
                                    <a href="#" class="btn btn-sm btn-light btn-active-light-primary">{{ $item->plan->interval == 'EVERY_30_DAYS' ? 'Monthly' : '' }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
