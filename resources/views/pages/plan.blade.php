@extends('shopify-app::layouts.default')

@extends('layout.default')

@section('toolbar')
<div class="toolbar py-5 py-lg-15" id="kt_toolbar">
</div>
@endsection


@section('wc-content')

@php
$shop = Auth::user();
$subscription = App\Models\Subscription::where('user_id','=',$shop->id)->first();
@endphp

@if($subscription)
<script type="text/javascript">
    window.location = "{{ route('dashboard') }}";
</script>
@endif


<div class="card" id="kt_pricing">
    <div class="card-body p-lg-17">
        <div class="d-flex flex-column">
            <div class="mb-13 text-center">
                <h1 class="fs-2hx fw-bold mb-5">Choose Your Plan</h1>

                <div class="text-gray-400 fw-semibold fs-5">
                    If you need more info about our pricing, please <a href="#" class="link-dark fw-bold">contact us</a>.
                </div>
            </div>

            <div class="nav-group nav-group-outline mx-auto mb-15" data-kt-buttons="true" data-kt-initialized="1">
                <button class="btn btn-color-gray-400 btn-active btn-active-secondary px-6 py-3 me-2 active" data-kt-plan="month">
                    Monthly
                </button>

                <button class="btn btn-color-gray-400 btn-active btn-active-secondary px-6 py-3" disabled readonly>
                    Annual
                </button>
            </div>

            <div class="row g-10">
                <div class="col-lg-4">
                    <div class="d-flex h-100 align-items-center">
                        <div class="w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-15 px-10">
                            <div class="mb-7 text-center">
                                <h1 class="text-dark mb-5 fw-bolder">Basic</h1>

                                <div class="text-gray-400 fw-semibold mb-5">
                                    Optimal for quick recipes<br> and quick designs
                                </div>

                                <div class="text-center">
                                    <span class="mb-2 text-info">$</span>

                                    <span class="fs-3x fw-bold text-info">
                                        20.00 </span>

                                    <span class="fs-7 fw-semibold opacity-50">/
                                        <span data-kt-element="period">Month</span>
                                    </span>
                                </div>
                            </div>

                            <div class="w-100 mb-10">
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        Made for small recipes </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        7 Days free trial </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        Add up to 5 recipes </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        Add up to 5 choices </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        1 Recipe List design </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        1 Recipe View design </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        Upto 3 products add </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                            </div>
                            <a href="{{ route('billing', ['plan' => 1, 'shop' => Auth::user()->name]) }}" class="btn btn-sm btn-info">Select</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex h-100 align-items-center">
                        <div class="w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-15 px-10">
                            <div class="mb-7 text-center">
                                <h1 class="text-dark mb-5 fw-bolder">Standard</h1>

                                <div class="text-gray-400 fw-semibold mb-5">
                                    Optimal for big recipes<br> and multiple designs options
                                </div>

                                <div class="text-center">
                                    <span class="mb-2 text-info">$</span>

                                    <span class="fs-3x fw-bold text-info">
                                        50.00 </span>

                                    <span class="fs-7 fw-semibold opacity-50">/
                                        <span data-kt-element="period">Month</span>
                                    </span>
                                </div>
                            </div>

                            <div class="w-100 mb-10">
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        Made for big recipes </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        7 Days free trial </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        Add unlimited recipes </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        Add unlimited choices </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        3 Recipe List design </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        3 Recipe View design </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        Upto 10 products add </span>
                                    <i class="fa-regular fa-circle-check"></i>
                                </div>
                            </div>
                            <a href="{{ route('billing', ['plan' => 2, 'shop' => Auth::user()->name]) }}" class="btn btn-sm btn-info">Select</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex h-100 align-items-center">
                        <div class="w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-15 px-10">
                            <div class="mb-7 text-center">
                                <h1 class="text-dark mb-5 fw-bolder">Premium</h1>
                                <div class="text-gray-400 fw-semibold mb-5">
                                    Optimal for enterprise <br> and advanced configurable
                                </div>
                                <div class="text-center">
                                    <span class="mb-2 text-info">$</span>
                                    <span class="fs-3x fw-bold text-info">
                                        70.00 </span>
                                    <span class="fs-7 fw-semibold opacity-50">/
                                        <span data-kt-element="period">Month</span>
                                    </span>
                                </div>
                            </div>
                            <div class="w-100 mb-10">
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        Made for enterprise recipes </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        7 Days free trial </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        Add unlimited recipes </span>
                                    <i class="fa-solid fa-circle-check "></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        Add unlimited choices </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-3">
                                        5 Recipe List design </span>
                                    <i class="fa-solid fa-circle-check"></i>
                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        5 Recipe View design </span>
                                    <i class="fa-solid fa-circle-check"></i>

                                </div>
                                <div class="d-flex align-items-center mb-5">
                                    <span class="fw-semibold fs-6 text-gray-800 flex-grow-1 pe-4">
                                        Unlimited products add </span>
                                    <i class="fa-solid fa-circle-check"></i>

                                </div>
                            </div>
                            <a href="{{ route('billing', ['plan' => 3, 'shop' => Auth::user()->name]) }}" class="btn btn-sm btn-info">Select</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection