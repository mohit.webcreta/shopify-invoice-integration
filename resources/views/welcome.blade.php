@extends('shopify-app::layouts.default')

@extends('layout.default')


@section('wc-content')

@php
// get auth data
$shop = Auth::user();

// get authenticated shop data
$shops = $shop->api()->rest('GET', '/admin/api/2023-04/shop.json');

$subscription = App\Models\Subscription::where('user_id','=',$shop->id)->first();
@endphp

@if($subscription)
<script type="text/javascript">
    window.location = "{{ route('dashboard') }}"; //here double curly bracket
</script>
@endif

<script type="text/javascript">
    window.location = "{{ route('plan') }}"; //here double curly bracket
</script>

@endsection





{{-- @section('scripts')
    @parent
    <script type="text/javascript">
        var AppBridge = window['app-bridge'];
        var actions = AppBridge.actions;
        var TitleBar = actions.TitleBar;
        var Button = actions.Button;
        var Redirect = actions.Redirect;
        var titleBarOptions = {
            title: 'Welcome',
        };
        var myTitleBar = TitleBar.create(app, titleBarOptions);
    </script>
@endsection --}}