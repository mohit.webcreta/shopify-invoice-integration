<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('recipes')->group(function () {
    Route::get('/{name}', [ApiController::class, 'fetchActivatedRecipes'])->name('api.recipes.fetchAll');
});

Route::prefix('recipe')->group(function () {
    Route::get('/{id}', [ApiController::class, 'fetchRecipe'])->name('api.recipes.fetchRecipe');
});

//Route::get('/pages/{url}', [RecipeController::class, 'recipesView'])->middleware(['verify.shopify'])->name('home');
