<?php

use App\Http\Controllers\ChoicesController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;
use App\Models\Plan;
use App\Models\User;
use App\Models\Charge;
use App\Models\Subscription;
use App\Models\StoreLog;
use Illuminate\Http\Request;




// user panel
route::middleware(['verify.shopify'])->group(function () {

    // index page
    Route::get('/', function () {
        return view('welcome');
    })->middleware(['billable'])->name('home');

    // app - plan page
    Route::get('/plan', function () {
        return view('pages.plan');
    })->name('plan');

    // plan post table
    Route::get('/plan/{type}', [ConfigController::class, 'storePlan'])->name('plan.store');

    // app - index page
    Route::get('/dashboard', [ConfigController::class, 'storeDashboard'])->name('dashboard');



    Route::get('app/billing/process/{id}', function (Request $request, $id) {
        // Get the shop information from the query string
        $shop = User::where('name', '=', $request->shop)->first();
        // get authenticated shop data
        $plans = Plan::where('id', $id)->first();

        $startDate = new DateTime();
        $oneMonthAfterDate = $startDate->add(new DateInterval('P1M'));
        $endDate = $oneMonthAfterDate->format('Y-m-d');

        $subscription = Subscription::where('user_id', '=', $shop->id)->orderBy('id', 'desc')->get();
        if (count($subscription) > 0) {
            StoreLog::updateOrCreate(
                [
                    'user_id' => $shop->id,
                    'name' => 'Plan',
                    'value' => 'An <span class="text-primary">existing</span> plan has been successfully <span class="text-success">upgraded </span> from <span class="text-info">' . $subscription[0]->plan->name . '</span> to <span class="text-info">' . $plans->name . '</span>.',
                    'created_at' => now()->format('H:i:s'),
                ]
            );
        } else {
            StoreLog::updateOrCreate(
                [
                    'user_id' => $shop->id,
                    'name' => 'Plan',
                    'value' => 'A <span class="text-success">new</span> plan named <span class="text-info">' . $plans->name . '</span> has been successfully <span class="text-primary">activated</span>.',
                    'created_at' => now()->format('H:i:s'),
                ]
            );
        }


        Subscription::updateOrCreate(
            [
                'plan_id' => $plans->id,
                'user_id' => $shop->id,
                'start_date' => new DateTime(),
                'end_date' => $endDate,
                'status' => 'active',
            ]
        );

        Charge::updateOrCreate(
            [
                'charge_id' => $request->charge_id,
                'test' => $plans->test,
                'status' => 1,
                'name' => $plans->name,
                'terms' => $plans->terms,
                'type' => $plans->type,
                'price' => $plans->price,
                'interval' => $plans->interval,
                'capped_amount' => $plans->capped_amount,
                'trial_days' => $plans->trial_days,
                'billing_on' => new DateTime(),
                'trial_ends_on' => null,
                'cancelled_on' => null,
                'expires_on' => $endDate,
                'plan_id' => $plans->id,
                'description' => null,
                'reference_charge' => null,
                'user_id' => $shop->id,
            ]
        );


        return view('pages.charges');
    });

    Route::get('/upload-page', [ConfigController::class, 'uploadLiquidPage'])->name('upload.page');


    Route::resource('/choices', ChoicesController::class)->except(['store', 'update', 'destroy']);
    Route::resource('/recipes', RecipeController::class)->except(['store', 'update', 'destroy']);

    Route::get('/pages/contact-support', [PagesController::class, 'contactSupportPage'])->name('pages.support'); // contact support page
    Route::get('/pages/manage-plan', [PagesController::class, 'managePlanPage'])->name('pages.manageplan'); // manage plan page
    Route::get('/pages/setting', [PagesController::class, 'manageUserSettings'])->name('pages.settings'); // manage setting page
    // Route::get('/pages/configuration', [PagesController::class, 'layoutConfigurationSettings'])->name('pages.config.layout'); // manage layouts

});

// Route::get('/authentication', [AdminController::class, 'authenticate'])->name('authenticate');

// admin panel
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();

    Route::get('/', [AdminController::class, 'index'])->name('admin.index'); // index page

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/home', [AdminController::class, 'storeDashboard'])->name('admin.dashboard'); // dashboard

        // users
        Route::get('/stores/list', [AdminController::class, 'getUsersList'])->name('admin.users.list');
        Route::get('/stores/logs/{id}', [AdminController::class, 'getUsersLogById'])->name('admin.users.storelog');

        // config
        Route::group(['prefix' => 'config'], function () {
            Route::get('/view', [AdminController::class, 'getConfigData'])->name('admin.config.view');
            Route::put('/{config}', [AdminController::class, 'updateConfigData'])->name('admin.config.update');
        });

        // plans
        Route::group(['prefix' => 'plans'], function () {
            Route::get('/index', [AdminController::class, 'getAllPlans'])->name('admin.plans.index');
            Route::get('/{plan}', [AdminController::class, 'getPlanData'])->name('admin.plans.edit');
            Route::put('/{plan}', [AdminController::class, 'updatePlanData'])->name('admin.plans.update');
        });

    });
});

Route::get('/403-page', [AdminController::class, 'accessDeniedPage'])->name('admin.page.denied'); // access denied page


Route::post('/choices', [ChoicesController::class, 'store'])->name('choices.store');
Route::put('/choices/{id}', [ChoicesController::class, 'update'])->name('choices.update');
Route::delete('/choices/{id}', [ChoicesController::class, 'destroy'])->name('choices.destroy');

Route::post('/recipes', [RecipeController::class, 'store'])->name('recipes.store');
Route::put('/recipes/{id}', [RecipeController::class, 'update'])->name('recipes.update');
Route::delete('/recipes/{id}', [RecipeController::class, 'destroy'])->name('recipes.destroy');